#!/bin/bash -e

cd voxel-converter
./gradlew build
cd ..
tar xvf voxel-converter/build/distributions/voxel-converter-1.0-SNAPSHOT.tar
for f in ../src/ply/*.ply
do
    ./voxel-converter-1.0-SNAPSHOT/bin/voxel-converter $f ../src/obj/
done