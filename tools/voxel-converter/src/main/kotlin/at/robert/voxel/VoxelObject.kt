package at.robert.voxel

sealed class VoxelMaterial {
    abstract val name: String
}

object EmptyVoxelMaterial : VoxelMaterial() {
    override val name = "invisible"
}

data class ColoredVoxelMaterial(val r: Int, val g: Int, val b: Int) : VoxelMaterial() {
    override val name = "voxel$r$g$b"
}

@ExperimentalUnsignedTypes
class VoxelObject(
    val xLength: Int,
    val yLength: Int,
    val zLength: Int,
    val data: UByteArray,
    val voxelMaterials: List<VoxelMaterial>
)
