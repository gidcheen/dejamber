package at.robert.voxel

import java.io.File

data class Int3(
    val x: Int,
    val y: Int,
    val z: Int,
)

data class Float3(
    val x: Float,
    val y: Float,
    val z: Float,
)

data class Float2(
    val x: Float,
    val y: Float,
) {
    operator fun times(d: Float) = Float2(x * d, y * d)
}

data class RectFace(
    val v1: Float3,
    val v2: Float3,
    val v3: Float3,
    val v4: Float3,
    val normal: Float3,
    val uv1: Float2,
    val uv2: Float2,
    val uv3: Float2,
    val uv4: Float2,
    val material: VoxelMaterial
)

val RectFace.triangles
    get() = listOf(
        Triangle(v1, v2, v4, uv1, uv2, uv4, normal, material),
        Triangle(v2, v3, v4, uv2, uv3, uv4, normal, material),
    )

data class Triangle(
    val v1: Float3,
    val v2: Float3,
    val v3: Float3,
    val uv1: Float2,
    val uv2: Float2,
    val uv3: Float2,
    val normal: Float3,
    val material: VoxelMaterial
)

val voxelSize = 1.0f
private val halfVoxel = voxelSize / 2f

@ExperimentalUnsignedTypes
fun VoxelObject.exportObj(file: File, noWrap: Boolean, scale: Float = 0.1f) {

    val faces = (0 until zLength).flatMap { z ->
        (0 until yLength).flatMap { y ->
            (0 until xLength).flatMap { x ->
                val material = data[x + y * xLength + z * xLength * yLength].let { voxelMaterials[it.toInt()] }
                if (material == EmptyVoxelMaterial) return@flatMap emptyList()

                val faces = listOf(
                    RectFace(
                        Float3(x + halfVoxel, y - halfVoxel, z + halfVoxel),
                        Float3(x + halfVoxel, y + halfVoxel, z + halfVoxel),
                        Float3(x + halfVoxel, y + halfVoxel, z - halfVoxel),
                        Float3(x + halfVoxel, y - halfVoxel, z - halfVoxel),
                        Float3(1f, 0f, 0f),
                        Float2(y - halfVoxel, z + halfVoxel),
                        Float2(y + halfVoxel, z + halfVoxel),
                        Float2(y + halfVoxel, z - halfVoxel),
                        Float2(y - halfVoxel, z - halfVoxel),
                        material
                    ) to Int3(1, 0, 0),
                    RectFace(
                        Float3(x - halfVoxel, y + halfVoxel, z + halfVoxel),
                        Float3(x - halfVoxel, y - halfVoxel, z + halfVoxel),
                        Float3(x - halfVoxel, y - halfVoxel, z - halfVoxel),
                        Float3(x - halfVoxel, y + halfVoxel, z - halfVoxel),
                        Float3(-1f, 0f, 0f),
                        Float2(y + halfVoxel, z + halfVoxel),
                        Float2(y - halfVoxel, z + halfVoxel),
                        Float2(y - halfVoxel, z - halfVoxel),
                        Float2(y + halfVoxel, z - halfVoxel),
                        material
                    ) to Int3(-1, 0, 0),
                    RectFace(
                        Float3(x + halfVoxel, y + halfVoxel, z + halfVoxel),
                        Float3(x - halfVoxel, y + halfVoxel, z + halfVoxel),
                        Float3(x - halfVoxel, y + halfVoxel, z - halfVoxel),
                        Float3(x + halfVoxel, y + halfVoxel, z - halfVoxel),
                        Float3(0f, 1f, 0f),
                        Float2(x + halfVoxel, z + halfVoxel),
                        Float2(x - halfVoxel, z + halfVoxel),
                        Float2(x - halfVoxel, z - halfVoxel),
                        Float2(x + halfVoxel, z - halfVoxel),
                        material
                    ) to Int3(0, 1, 0),
                    RectFace(
                        Float3(x - halfVoxel, y - halfVoxel, z + halfVoxel),
                        Float3(x + halfVoxel, y - halfVoxel, z + halfVoxel),
                        Float3(x + halfVoxel, y - halfVoxel, z - halfVoxel),
                        Float3(x - halfVoxel, y - halfVoxel, z - halfVoxel),
                        Float3(0f, -1f, 0f),
                        Float2(x - halfVoxel, z + halfVoxel),
                        Float2(x + halfVoxel, z + halfVoxel),
                        Float2(x + halfVoxel, z - halfVoxel),
                        Float2(x - halfVoxel, z - halfVoxel),
                        material
                    ) to Int3(0, -1, 0),
                    RectFace(
                        Float3(x - halfVoxel, y + halfVoxel, z + halfVoxel),
                        Float3(x + halfVoxel, y + halfVoxel, z + halfVoxel),
                        Float3(x + halfVoxel, y - halfVoxel, z + halfVoxel),
                        Float3(x - halfVoxel, y - halfVoxel, z + halfVoxel),
                        Float3(0f, 0f, 1f),
                        Float2(x - halfVoxel, y + halfVoxel),
                        Float2(x + halfVoxel, y + halfVoxel),
                        Float2(x + halfVoxel, y - halfVoxel),
                        Float2(x - halfVoxel, y - halfVoxel),
                        material
                    ) to Int3(0, 0, 1),
                    RectFace(
                        Float3(x - halfVoxel, y - halfVoxel, z - halfVoxel),
                        Float3(x + halfVoxel, y - halfVoxel, z - halfVoxel),
                        Float3(x + halfVoxel, y + halfVoxel, z - halfVoxel),
                        Float3(x - halfVoxel, y + halfVoxel, z - halfVoxel),
                        Float3(0f, 0f, -1f),
                        Float2(x - halfVoxel, y - halfVoxel),
                        Float2(x + halfVoxel, y - halfVoxel),
                        Float2(x + halfVoxel, y + halfVoxel),
                        Float2(x - halfVoxel, y + halfVoxel),
                        material
                    ) to Int3(0, 0, -1),
                ).map {
                    it.first.fixCoordinateSystem() to it.second
                }.filter { (_, d) ->
                    val compareX = x + d.x
                    val compareY = y + d.y
                    val compareZ = z + d.z

                    if (compareX < 0 || compareX >= xLength) return@filter true
                    if (compareY < 0 || compareY >= yLength) return@filter true
                    if (compareZ < 0 || compareZ >= zLength) return@filter true

                    val otherMat =
                        this.data[compareX + compareY * xLength + compareZ * xLength * yLength].let { this.voxelMaterials[it.toInt()] }
                    if (otherMat == EmptyVoxelMaterial) return@filter true

                    false
                }.map { it.first }
                faces
            }
        }
    }

    val triangles = faces.flatMap { it.triangles }

    println("Got ${triangles.size} triangles (${faces.size} faces)")

    val objFile = File(file.parentFile, file.nameWithoutExtension + ".obj")
    val objSb = StringBuilder()

    val texCoords = triangles.flatMap { listOf(it.uv1, it.uv2, it.uv3) }.distinct()
    val tI = texCoords.toIndexMap()
    val normals = triangles.map { it.normal }.distinct()
    val nI = normals.toIndexMap()
    val verts = triangles.flatMap { listOf(it.v1, it.v2, it.v3) }.distinct()
    val vI = verts.toIndexMap()

    objSb.appendLine("# by Robert")
    objSb.appendLine("# group")
    objSb.appendLine("o ${file.nameWithoutExtension}")
    objSb.appendLine("#material")
    objSb.appendLine("mtllib ${File(file.parentFile, file.nameWithoutExtension + ".mtl")}")
    objSb.appendLine("#normals")
    normals.forEach {
        objSb.appendLine("vn ${it.x} ${it.y} ${it.z}")
    }
    objSb.appendLine("#texcoords")
    texCoords.let {
        if (noWrap)
            it.normalize()
        else
            it.map { it * 0.05f }
    }.forEach {
        objSb.appendLine("vt ${it.x} ${it.y}")
    }
    objSb.appendLine("#verts")
    verts.centered().map { Float3(it.x * scale, it.y * scale, it.z * scale) }.forEach {
        objSb.appendLine("v ${it.x} ${it.y} ${it.z}")
    }
    objSb.appendLine("#faces")
    triangles.groupBy { it.material }.forEach { (mat, tris) ->
        objSb.appendLine("usemtl ${mat.name}")
        tris.forEach { tri ->
            objSb.appendLine(
                "f ${vI[tri.v1]!! + 1}/${tI[tri.uv1]!! + 1}/${nI[tri.normal]!! + 1} " +
                        "${vI[tri.v2]!! + 1}/${tI[tri.uv2]!! + 1}/${nI[tri.normal]!! + 1} " +
                        "${vI[tri.v3]!! + 1}/${tI[tri.uv3]!! + 1}/${nI[tri.normal]!! + 1} "
            )
        }
    }
    println("Using materials:")
    triangles.map { it.material.name }.distinct().sorted().forEach {
        println("\t$it")
    }

    objFile.writeText(objSb.toString())
}

private fun List<Float3>.centered(): List<Float3> {
    val xs = map { it.x }
    val ys = map { it.y }
    val zs = map { it.z }
    val minX = xs.minOrNull() ?: return this
    val minY = ys.minOrNull()!!
    val minZ = zs.minOrNull()!!
    val maxX = xs.maxOrNull()!!
    val maxY = ys.maxOrNull()!!
    val maxZ = zs.maxOrNull()!!
    val lengthX = maxX - minX
    val lengthY = maxY - minY
    val lengthZ = maxZ - minZ

    return map {
        Float3(
            it.x - minX - lengthX / 2f,
            it.y - minY - lengthY / 2f,
            it.z - minZ - lengthZ / 2f,
        )
    }
}

private fun List<Float2>.normalize(): List<Float2> {
    val xs = map { it.x }
    val ys = map { it.y }
    val minX = xs.minOrNull() ?: return this
    val minY = ys.minOrNull()!!
    val maxX = xs.maxOrNull()!!
    val maxY = ys.maxOrNull()!!

    val width = maxX - minX
    val height = maxY - minY

    return this.map {
        Float2(
            (it.x - minX) / width,
            (it.y - minY) / height,
        )
    }
}

private fun RectFace.fixCoordinateSystem(): RectFace {
    return this.copy(
        v4 = v1.fixCoordinateSystem(),
        v3 = v2.fixCoordinateSystem(),
        v2 = v3.fixCoordinateSystem(),
        v1 = v4.fixCoordinateSystem(),
        uv4 = uv1,
        uv3 = uv2,
        uv2 = uv3,
        uv1 = uv4,
        normal = normal.fixCoordinateSystem()
    )
}

private fun Float3.fixCoordinateSystem(): Float3 = Float3(-x, z, y)

private fun <E> List<E>.toIndexMap(): Map<E, Int> = mapIndexed { index, value -> value to index }.toMap()
