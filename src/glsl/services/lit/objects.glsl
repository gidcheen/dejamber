struct Light
{
	mat4 world_to_local;
	mat4 local_to_world;
	vec4 color;
	int type;
	float cutoff_inner;
	float cutoff_outer;
	int _pad;
};
struct Surface
{
	mat4 local_to_world;
};
struct Camera
{
	mat4 world_to_local;
	mat4 local_to_world;
	mat4 local_to_projection;
};

int light_type_ambient = 0;
int light_type_directional = 1;
int light_type_point = 2;
int light_type_spot = 3;

layout(std140, set = 0, binding = 0) buffer Lights
{
	Light lights[1024];
};
layout(std140, set = 0, binding = 1) buffer Surfaces
{
	Surface surfaces[1024];
};
layout(std140, set = 0, binding = 2) buffer Cameras
{
	Camera cameras[1024];
};

layout(std140, push_constant) uniform ObjectIndices
{
	uint lights_co_00;
	uint lights_01_02;
	uint lights_03_04;
	uint lights_05_04;
	uint lights_07_04;
	uint lights_09_10;
    uint surface_camera;
} object_indices;

uint GetObjectIndex(uint object_index, uint index)
{
	return index == 0
		? (object_index) & 0xFFFF
		: (object_index >> 16) & 0xFFFF;
}

uint GetLightCount()
{
	return object_indices.lights_co_00 & 0xFFFF;
}

Light GetLight(uint index)
{
	switch(index)
	{
		case 0: return lights[GetObjectIndex(object_indices.lights_co_00, 1)];
		case 1: return lights[GetObjectIndex(object_indices.lights_01_02, 0)];
		case 2: return lights[GetObjectIndex(object_indices.lights_01_02, 1)];
		case 3: return lights[GetObjectIndex(object_indices.lights_03_04, 0)];
		case 4: return lights[GetObjectIndex(object_indices.lights_03_04, 1)];
		case 5: return lights[GetObjectIndex(object_indices.lights_05_04, 0)];
		case 6: return lights[GetObjectIndex(object_indices.lights_05_04, 1)];
		case 7: return lights[GetObjectIndex(object_indices.lights_07_04, 0)];
		case 8: return lights[GetObjectIndex(object_indices.lights_07_04, 1)];
		case 9: return lights[GetObjectIndex(object_indices.lights_09_10, 0)];
		case 10: return lights[GetObjectIndex(object_indices.lights_09_10, 1)];
	}
}

Surface GetSurface()
{
	return surfaces[GetObjectIndex(object_indices.surface_camera, 0)];
}

Camera GetCamera()
{
	return cameras[GetObjectIndex(object_indices.surface_camera, 1)];
}
