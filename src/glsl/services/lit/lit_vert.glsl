#include "lit_vert_input.glsl"
#include "lit_vert_output.glsl"

void main()
{
	Surface surface = GetSurface();
	Camera camera = GetCamera();

	vec4 world_position = surface.local_to_world * vec4(position, 1.0);

	vert_to_frag.position = vec3(world_position);
	vert_to_frag.color = color;
	vert_to_frag.uv = uv;

	mat4 normalized_local_to_world = transpose(inverse(surface.local_to_world));
	vec3 t = normalize(vec3(normalized_local_to_world * vec4(tangent,   0.0)));
	vec3 b = normalize(vec3(normalized_local_to_world * vec4(bitangent, 0.0)));
	vec3 n = normalize(vec3(normalized_local_to_world * vec4(normal,    0.0)));
	vert_to_frag.tbn = mat3(t, b, n);

	vec4 camera_position = camera.world_to_local * world_position;
	gl_Position = camera.local_to_projection * camera_position;
}
