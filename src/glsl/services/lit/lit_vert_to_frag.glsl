struct VertToFrag
{
	vec3 position;
	vec3 color;
	vec2 uv;
	mat3 tbn;
};
