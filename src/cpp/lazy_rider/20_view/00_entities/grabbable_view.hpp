#pragma once

#include <memory>

#include <loading/cache.hpp>
#include <loading/scene_instance.hpp>
#include <scene/scene.hpp>

#include <10_domain/00_entities/grabable.hpp>
#include <10_domain/03_game/game.hpp>

using namespace gid_tech;
using namespace gid_tech::scene;
using loading::LoadingCache;
using loading::SceneInstance;

namespace lazy_rider::view::entities
{
	using namespace domain::entities;
	using namespace domain::game;

	class GrabbableView
	{
	private:
		Grabable const & grabbable;

		Node root;
		SceneInstance instance;

	public:
		GrabbableView(Grabable const & grabbable, Scene & scene, LoadingCache & loading_cache);

		Grabable const & GetGrabbable() const;

		void Tick();
	};
}
