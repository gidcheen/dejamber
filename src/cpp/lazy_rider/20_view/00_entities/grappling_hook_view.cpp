#include "grappling_hook_view.hpp"

#include <algebra/transform.hpp>

using namespace gid_tech::algebra;

namespace lazy_rider::view::entities
{
	GrapplingHookView::GrapplingHookView(Player const & player, GrapplingHook const & grappling_hook, Scene & scene, LoadingCache & loading_cache) :
		player(player),
		grappling_hook(grappling_hook),
		root("hook", {}, &scene),
		grappling_hook_scene(loading::InstantiateSceneElements(
			loading_cache.GetOrLoadScene("gltf/rope/rope.gltf"),
			&root)),
		rope(get<0>(grappling_hook_scene)[0]->FindChild("Rope").Value())
	{
	}

	void GrapplingHookView::Tick()
	{
		auto hook_pos = grappling_hook.Position();
		auto player_pos = player.Position();
		root.SetPosition({hook_pos.x, 0, hook_pos.y});

		auto dir = hook_pos - player_pos;
		auto forward = Vector<f32, 2>{0, 1};
		auto angle = acos(dir.Normalized().Dot(forward));
		if (dir.x > 0)
			angle = angle * -1;
		root.SetRotation(Quaternion<f32>::FromAxisAngle({0, 1, 0}, Transform<f32>::ToRadians(180) - angle));
		rope->SetScale({1, 1, dir.Length()});
	}
}
