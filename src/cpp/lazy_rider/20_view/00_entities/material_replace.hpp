#pragma once

#include <loading/cache.hpp>
#include <scene/model_node.hpp>

using namespace gid_tech::scene;
using namespace gid_tech::loading;

namespace lazy_rider::view::entities
{
	void ReplaceMaterials(ModelNode * node, LoadingCache & loading_cache);
}
