#include "player_view.hpp"

#include <algebra/transform.hpp>
#include <cmath>
#include <iostream>
#include <loading/material_data.hpp>
#include <scene/model_node.hpp>

#include "material_replace.hpp"

using namespace algebra;

using gid_tech::windowing::Input;

namespace lazy_rider::view::entities
{
	PlayerView::PlayerView(Game & game, Player const & player, Scene & scene, shared_ptr<rendering::Frame> frame, LoadingCache & loading_cache) :
		game(game),
		player(player),
		loading_cache(loading_cache),
		root("root", {}, &scene),
		astronaut_root("astronaut root", {}, &root),
		camera_y("camera_y", {}, &root),
		camera_x(
			"camera_x",
			Node::Transform{
				.rotation = Quaternion<f32>::FromAxisAngle({1, 0, 0}, Transform<f32>::ToRadians(-40)),
			},
			&camera_y),
		camera(
			"main camera",
			Node::Transform{.translation = {0, 0, 13}},
			&camera_x,
			scene::CameraNode::Config{
				.fov = Transform<f32>::ToRadians(90),
				.apsect = (f32) frame->GetSize().width / (f32) frame->GetSize().height,
				.near = 0.1,
				.far = 310,
			},
			frame),
		skybox_root(
			"skybox_root",
			Node::Transform{
				.rotation = Quaternion<f32>::FromAxisAngle({1, 0, 0}, Transform<f32>::ToRadians(45)),
				.scale = {150, 150, 150},
			},
			&root),
		helmet_light(
			"helmet_light",
			Node::Transform{.translation = {0.3, 1.5, -1.2}},
			&astronaut_root,
			LightNode::Config{.type = LightNode::Config::Type::Point, .color = {4.0, 7.0, 10.0}}),
		astronaut(new SceneInstance(loading::InstantiateSceneElements(
			loading_cache.GetOrLoadScene("gltf/astronaut.gltf"),
			Optional<variant<Scene *, Node *>>(&astronaut_root)))),
		skybox(loading::InstantiateSceneElements(
			loading_cache.GetOrLoadScene("gltf/skybox/skybox.gltf"),
			Optional<variant<Scene *, Node *>>(&skybox_root)))
	{
		ReplaceMaterials(dynamic_cast<ModelNode *>(astronaut_root.FindChild("astronaut_lying").Value()), loading_cache);
	}

	void PlayerView::Tick(f32 dt, Input const & input)
	{
		auto pos = player.Position();
		root.SetPosition({pos.x, 0, pos.y});
		astronaut_root.SetRotation(Quaternion<f32>::FromAxisAngle({0, 1, 0}, player.Rotation()));

		// win
		if (player.hasWon && astronaut_won.IsNone())
		{
			astronaut_won.Emplace(new SceneInstance(loading::InstantiateSceneElements(
				loading_cache.GetOrLoadScene("gltf/astronaut_won.gltf"),
				Optional<variant<Scene *, Node *>>(&astronaut_root))));
			astronaut.Clear();
			drinks.clear();
			trashes.clear();
			ReplaceMaterials(dynamic_cast<ModelNode *>(astronaut_root.FindChild("you_won").Value()), loading_cache);
		}

		// input
		if (input.GetMouseButtonDown(Input::Mouse::Button::Left))
		{
			auto pos = GetMouseFloorPlanePoint(input);
			if (pos.IsSome())
				game.AddPlayerThrow(pos.Value());
		}

		if (input.GetMouseButtonDown(Input::Mouse::Button::Right))
		{
			auto pos = GetMouseFloorPlanePoint(input);
			if (pos.IsSome())
				game.AddPlayerGrapple(pos.Value());

			auto & grapplingHook = game.GetGrapplingHook();
			if (grapplingHook.IsSome())
			{
				game.AddGrapplingHookStopFlight();
				if (grapplingHook.Value().IsAttached())
					game.AddGrapplingHookDetach();
			}
		}

		if (input.GetMouseButton(Input::Mouse::Button::Middle))
		{
			auto input_mouse_delta = input.GetMousePositionDelta();
			auto frame_size = camera.GetFrame().GetSize();
			Vector<f32, 2> mouse_delta{(f32) input_mouse_delta.x / (f32) frame_size.width, (f32) input_mouse_delta.y / (f32) frame_size.width};

			static f32 const speed = 20;

			if (input_mouse_delta.x != 0)
				camera_y.Rotate(Quaternion<f32>::FromAxisAngle({0, 1, 0}, -mouse_delta.x * speed));
			if (input_mouse_delta.y != 0)
			{
				camera_x.Rotate(Quaternion<f32>::FromAxisAngle({1, 0, 0}, -mouse_delta.y * speed));
				auto euler = Transform<f32>::QuaternionToEuler(camera_x.GetRotation());
				euler.x = min(max(euler.x, Transform<f32>::ToRadians(-80.f)), Transform<f32>::ToRadians(-20.f));
				camera_x.SetRotation(Quaternion<f32>::FromAxisAngle({1, 0, 0}, euler.x));
			}
		}

		auto scroll_delta = input.GetMouseWheelDelta();
		if (scroll_delta != 0)
		{
			auto pos = camera.GetPosition();
			pos.z -= scroll_delta;
			pos.z = min(max(pos.z, 5.f), 20.f);
			camera.SetPosition(pos);
		}

		// inventory
		if (!player.hasWon)
		{
			while (drinks.size() < player.drinkInventory.size())
			{
				drinks.emplace_back(make_unique<SceneInstance>(loading::InstantiateSceneElements(
					loading_cache.GetOrLoadScene("gltf/grabbable/margarita.gltf"),
					Optional<variant<Scene *, Node *>>(&astronaut_root))));
				auto & [root_nodes, _, __] = **(drinks.end() - 1);
				for (auto & root_node : root_nodes)
				{
					root_node->SetPosition({0.5, 0.2f + (f32) drinks.size() - 1, -2.5});
					ReplaceMaterials(dynamic_cast<ModelNode *>(root_node), loading_cache);
				}
			}
			while (drinks.size() > player.drinkInventory.size())
				drinks.pop_back();

			while (trashes.size() < player.trashInventory.size())
			{
				trashes.emplace_back(make_unique<SceneInstance>(loading::InstantiateSceneElements(
					loading_cache.GetOrLoadScene("gltf/grabbable/asteroid.gltf"),
					Optional<variant<Scene *, Node *>>(&astronaut_root))));
				auto & [root_nodes, _, __] = **(trashes.end() - 1);
				for (auto & root_node : root_nodes)
				{
					root_node->SetPosition({0.5, 0.1f + (f32) trashes.size() - 1, 0.2});
					ReplaceMaterials(dynamic_cast<ModelNode *>(root_node), loading_cache);
				}
			}
			while (trashes.size() > player.trashInventory.size())
				trashes.pop_back();
		}
	}

	void PlayerView::ChangeAspect(f32 aspect)
	{
		auto config = camera.GetConfig();
		config.apsect = aspect;
		camera.SetConfig(config);
	}

	Optional<Vector<f32, 2>> PlayerView::GetMouseFloorPlanePoint(Input const & input)
	{
		auto frame_size = camera.GetFrame().GetSize();
		Vector<f32, 4> mouse_frame_pos{
			(f32) input.GetMousePosition().x / (f32) frame_size.width * 2 - 1,
			(f32) input.GetMousePosition().y / (f32) frame_size.height * 2 - 1,
			1,
			1,
		};
		auto inverse_projection = camera.GetLocalToWorld() * camera.GetLocalToProjection().Inverse();
		auto mouse_world_pos = inverse_projection * mouse_frame_pos;
		mouse_world_pos /= mouse_world_pos.w;
		auto mouse_world_dir = mouse_world_pos - camera.GetWorldPosition();

		if (mouse_world_dir.y > 0)
			return OptionalNone;

		auto pos = IntersectFloor(camera.GetWorldPosition(), mouse_world_dir);
		if (pos.IsSome())
		{
			auto p = Vector<f32, 2>{pos.Value().x, pos.Value().z};
			p = player.Position() - p;
			return Optional<Vector<f32, 2>>(p);
		}
		else
			return OptionalNone;
	}

	Optional<Vector<f32, 3>> PlayerView::IntersectFloor(Vector<f32, 3> origin, Vector<f32, 3> dir)
	{
		auto a = (Vector<f32, 3>{} - origin).Dot({0, 1, 0});
		auto b = dir.Dot({0, 1, 0});

		if (a == 0 && b == 0)
			return Optional<Vector<f32, 3>>(origin);
		else if (b != 0)
		{
			auto length = a / b;
			Vector<f32, 3> vector = dir * length;
			return Optional<Vector<f32, 3>>(origin + vector);
		}
		else
			return OptionalNone;
	}
}
