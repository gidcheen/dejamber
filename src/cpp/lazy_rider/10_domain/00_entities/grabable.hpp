#pragma once

#include "../services/algebra/vector.hpp"
#include "drink.hpp"
#include "trash.hpp"
#include <functional>
#include <memory>
#include <variant>

class b2Body;
class b2Fixture;
class b2World;

namespace lazy_rider::domain::entities
{
	using gid_tech::algebra::Vector;

	struct Grabable
	{
		std::unique_ptr<b2Body, std::function<void(b2Body *)>> body;
		Grabable(b2World & physics_world, Vector<f32, 2> initalPosition, Vector<f32, 2> initalVelocity, std::variant<Trash, Drink> object);

		std::variant<Trash, Drink> object;

		Vector<f32, 2> Position() const;
		void Position(Vector<f32, 2>);
		f32 Rotation() const;
		void Rotation(f32);
	};
}