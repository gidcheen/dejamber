#include "player.hpp"

#include "box2d/box2d.h"

namespace lazy_rider::domain::entities
{
	Player::Player(b2World & physics_world)
	{
		b2BodyDef bodyDef;
		bodyDef.type = b2_dynamicBody;
		bodyDef.position.Set(0.0f, 0.0f);
		body = std::unique_ptr<b2Body, std::function<void(b2Body *)>>{
			physics_world.CreateBody(&bodyDef),
			[&physics_world](b2Body * b) {
				physics_world.DestroyBody(b);
			}};

		b2PolygonShape dynamicBox;
		dynamicBox.SetAsBox(2.0f, 2.0f);

		b2FixtureDef fixtureDef;
		fixtureDef.shape = &dynamicBox;
		fixtureDef.density = 1.0f;
		fixtureDef.friction = 0.3f;

		body->CreateFixture(&fixtureDef);
		body->GetUserData().anyObject = this;
	}

	Vector<f32, 2> Player::Position() const
	{
		auto physicsPosition = body->GetPosition();
		return {physicsPosition.x, physicsPosition.y};
	}

	void Player::Position(Vector<f32, 2> position)
	{
		auto angle = body->GetAngle();
		body->SetTransform(b2Vec2{position.x, position.y}, angle);
	}

	f32 Player::Rotation() const
	{
		return body->GetAngle();
	}

	void Player::Rotation(f32 angle)
	{
		auto physicsPosition = body->GetPosition();
		body->SetTransform(physicsPosition, angle);
	}

	void Player::ApplyImpulse(Vector<f32, 2> impulseForce)
	{
		body->ApplyLinearImpulseToCenter(b2Vec2{impulseForce.x, impulseForce.y}, true);
	}

	Vector<f32, 2> Player::Velocity() const
	{
		auto bodyVelocity = body->GetLinearVelocity();
		return {bodyVelocity.x, bodyVelocity.y};
	}

	void Player::AddTrash(Trash trash)
	{
		trashInventory.emplace_back(trash);
	}

	void Player::AddDrink(Drink drink)
	{
		drinkInventory.emplace_back(drink);
	}

	Optional<Trash> Player::PopTrash()
	{
		if (trashInventory.empty())
			return {};

		auto trash = trashInventory.back();
		trashInventory.pop_back();

		return trash;
	}
}
