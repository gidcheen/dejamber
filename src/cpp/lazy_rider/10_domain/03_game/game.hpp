#pragma once

#include <aggregates/optional.hpp>
#include <box2d/b2_world.h>
#include <box2d/b2_world_callbacks.h>
#include <queue>
#include <vector>

#include "../00_entities/grabable.hpp"
#include "../00_entities/grappling_hook.hpp"
#include "../00_entities/player.hpp"
#include "../01_systems/contact_resolver.hpp"
#include "../01_systems/grabable_spawner.hpp"
#include "../01_systems/grapplingHook_pull.hpp"
#include "../01_systems/player_movement.hpp"
#include "../01_systems/win_condition.hpp"
#include "../02_events/grabable_interact.hpp"
#include "../02_events/grapplingHook_interact.hpp"
#include "../02_events/player_interact.hpp"

class b2DestroyHelper : public b2DestructionListener
{
	void SayGoodbye(b2Joint * joint) override;
	void SayGoodbye(b2Fixture * fixture) override;
};

using namespace std;
using namespace gid_tech::aggregates;

namespace lazy_rider::domain::game
{
	using namespace entities;
	using namespace systems;
	using namespace events;

	class Game
	{
	private:
		bool should_run = true;
		b2World physics_world;
		b2DestroyHelper destroyHelper;

		// entities
		Player player;
		Optional<GrapplingHook> grapplingHook;
		std::vector<std::unique_ptr<Grabable>> grabables;

		// systems
		// PlayerMovement playerMovement;
		ContactResolver contactResolver;
		GrapplingHookPull grapplingHookPull;
		GrabableSpawner grabableSpawner;
		WinCondition winCondition;

		// events
		queue<GrabableRemove> grabable_remove;

		queue<PlayerGrapple> player_grapple;
		queue<PlayerThrow> player_throws;
		queue<PlayerPickUpTrash> player_trashpickups;
		queue<PlayerPickUpDrink> player_drinkpickups;

		queue<GrapplingHookAttach> grapplingHook_attach;
		queue<GrapplingHookDetach> grapplingHook_detach;
		queue<GrapplingHookStopFlight> grapplingHook_stopFlight;
		queue<GrapplingHookDestroy> grapplingHook_destroy;

	public:
		Game();

		Player const & GetPlayer() const;
		Optional<GrapplingHook> const & GetGrapplingHook() const;
		std::vector<std::unique_ptr<Grabable>> const & GetGrabbables() const;

		void Tick(f32 dt);
		bool ShouldRun();
		void Stop();

		template<class Event, class... HandleArgs>
		void HandleEvents(queue<Event> & events, HandleArgs &&... args)
		{
			while (!events.empty())
			{
				events.front().Handle(std::forward<HandleArgs>(args)...);
				events.pop();
			}
		}

		template<class... PlayerGrappleArgs>
		void AddPlayerGrapple(PlayerGrappleArgs &&... args)
		{
			player_grapple.emplace(std::forward<PlayerGrappleArgs>(args)...);
		}

		template<class... PlayerThrowArgs>
		void AddPlayerThrow(PlayerThrowArgs &&... args)
		{
			player_throws.emplace(std::forward<PlayerThrowArgs>(args)...);
		}

		template<class... PlayerPickUpArgs>
		void AddPlayerTrashPickup(PlayerPickUpArgs &&... args)
		{
			player_trashpickups.emplace(std::forward<PlayerPickUpArgs>(args)...);
		}

		template<class... PlayerPickUpArgs>
		void AddPlayerDrinkPickup(PlayerPickUpArgs &&... args)
		{
			player_drinkpickups.emplace(std::forward<PlayerPickUpArgs>(args)...);
		}

		template<class... GrabableArgs>
		void AddGrabable(GrabableArgs &&... args)
		{
			grabables.emplace_back(std::make_unique<Grabable>(physics_world, std::forward<GrabableArgs>(args)...));
		}

		template<class... GrabableArgs>
		void AddRemoveGrabable(GrabableArgs &&... args)
		{
			grabable_remove.emplace(std::forward<GrabableArgs>(args)...);
		}

		template<class... GrapplingHookAttachArgs>
		void AddGrapplingHookAttach(GrapplingHookAttachArgs &&... args)
		{
			grapplingHook_attach.emplace(std::forward<GrapplingHookAttachArgs>(args)...);
		}

		template<class... GrapplingHookDetachArgs>
		void AddGrapplingHookDetach(GrapplingHookDetachArgs &&... args)
		{
			grapplingHook_detach.emplace(std::forward<GrapplingHookDetachArgs>(args)...);
		}

		template<class... GrapplingHookDestroArgs>
		void AddGrapplingHookDestroy(GrapplingHookDestroArgs &&... args)
		{
			grapplingHook_destroy.emplace(std::forward<GrapplingHookDestroArgs>(args)...);
		}

		template<class... GrapplingHookStopFlightArgs>
		void AddGrapplingHookStopFlight(GrapplingHookStopFlightArgs &&... args)
		{
			grapplingHook_stopFlight.emplace(std::forward<GrapplingHookStopFlightArgs>(args)...);
		}

		void RemoveGrabable(Grabable *);
	};
}
