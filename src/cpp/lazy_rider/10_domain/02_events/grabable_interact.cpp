#include "grabable_interact.hpp"
#include "../00_entities/grabable.hpp"
#include "../03_game/game.hpp"

namespace lazy_rider::domain::events
{
    GrabableRemove::GrabableRemove(Grabable* grabable):grabable(grabable)
    {}

    void GrabableRemove::Handle(Game& game)
    {
        game.RemoveGrabable(grabable);
    }
}