#pragma once

#include "../00_entities/drink.hpp"
#include "../00_entities/trash.hpp"
#include "../services/algebra/vector.hpp"
#include <functional>
#include <memory>
#include <random>
#include <vector>

class b2Joint;
class b2World;

namespace lazy_rider::domain::game
{
	class Game;
}

namespace lazy_rider::domain::entities
{
	struct Player;
	struct Grabable;
}

namespace lazy_rider::domain::systems
{
	using namespace lazy_rider::domain::entities;
	using namespace lazy_rider::domain::game;
	using gid_tech::algebra::Vector;

	class GrabableSpawner
	{
	private:
		f32 minRadius{};
		f32 maxRadius{};
		f32 trashChance{};
		f32 drinkChance{};
		f32 spawnTries{};
		const f32 spawnIntervalInSeconds{};
		std::vector<Trash> trashPrototypes;
		Drink drinkPtorotype{};

		f32 timeTillNextSpawn{};

		std::mt19937 mt;

	public:
		GrabableSpawner(f32 minRadius, f32 maxRadius, f32 trashChance, f32 drinkChance, f32 spawnTries, f32 spawnInterval, std::vector<Trash> trashPrototypes, Drink drinkPtorotype);
		void Tick(f32 dt, Game & game, Player & player);
	};
}