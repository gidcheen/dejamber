#pragma once


namespace lazy_rider::domain::game
{
    class Game;
}

namespace lazy_rider::domain::entities
{
    struct Player;
}

namespace lazy_rider::domain::systems
{
	using namespace lazy_rider::domain::entities;

    class WinCondition
    {
    private:
        int trashToCollect;
        int drinksToCollect;
    public:
        WinCondition(int trashToCollect,int drinksToCollect);
        void Tick(Player& player);
    };   
}