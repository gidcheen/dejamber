#pragma once

#include <rendering/data/color.hpp>

namespace gid_tech::loading
{
	using namespace rendering;
	
	struct MaterialData
	{
		data::Color color{1, 1, 1, 1};
		f32 metalicness{1};
		f32 roughness{0};
	};
}
