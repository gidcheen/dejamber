#pragma once

#include <filesystem>
#include <functional>
#include <memory>

#include <rendering/resources/pipeline.hpp>

namespace gid_tech::loading::pipeline
{
	using namespace std;
	using namespace filesystem;
	using namespace rendering;

	shared_ptr<Pipeline> Load(
		path path_vert,
		path path_frag,
		u32 image_count,
		bool is_transparent,
		RenderManager & render_manager);
}
