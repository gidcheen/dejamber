#pragma once

#include "slice.hpp"

namespace gid_tech::collections
{
	template<typename T>
	Slice<T>::Slice(usize count, T * elements) :
		count{count},
		elements{elements}
	{
	}

	template<typename T>
	T & Slice<T>::operator[](usize index)
	{
		return elements[index];
	}

	template<typename T>
	T const & Slice<T>::operator[](usize index) const
	{
		return elements[index];
	}

	template<typename T>
	Slice<T> Slice<T>::operator[](Range range)
	{
		return {range.Count(), &elements[range.start]};
	}

	template<typename T>
	Slice<T const> Slice<T>::operator[](Range range) const
	{
		return {range.Count(), &elements[range.start]};
	}

	template<typename T>
	b8 Slice<T>::operator==(Slice<T> const & other) const
	{
		if (count == other.count)
		{
			for (usize i{}; i < count; i++)
			{
				if (elements[i] == other.elements[i])
					continue;
				return false;
			}
			return true;
		}
		else
		{
			return false;
		}
	}

	template<typename T>
	b8 Slice<T>::operator!=(Slice<T> const & other) const
	{
		if (count != other.count)
		{
			for (usize i{}; i < count; i++)
			{
				if (elements[i] != other.elements[i])
					continue;
				return false;
			}
			return true;
		}
		else
		{
			return false;
		}
	}

	template<typename T>
	usize Slice<T>::Count() const
	{
		return count;
	}

	template<typename T>
	b8 Slice<T>::IsEmpty() const
	{
		return count == 0;
	}

	template<typename T>
	T & Slice<T>::First()
	{
		return elements[0];
	}

	template<typename T>
	T const & Slice<T>::First() const
	{
		return elements[0];
	}

	template<typename T>
	T & Slice<T>::Last()
	{
		return elements[count - 1];
	}

	template<typename T>
	T const & Slice<T>::Last() const
	{
		return elements[count - 1];
	}

	template<typename T>
	IteratorForward<T> Slice<T>::Begin()
	{
		return {&First()};
	}

	template<typename T>
	IteratorForward<T const> Slice<T>::Begin() const
	{
		return {&First()};
	}

	template<typename T>
	IteratorForward<T> Slice<T>::End()
	{
		return {&Last() + 1};
	}

	template<typename T>
	IteratorForward<T const> Slice<T>::End() const
	{
		return {&Last() + 1};
	}

	template<typename T>
	IteratorBackward<T> Slice<T>::RBegin()
	{
		return {&Last()};
	}

	template<typename T>
	IteratorBackward<T const> Slice<T>::RBegin() const
	{
		return {&Last()};
	}

	template<typename T>
	IteratorBackward<T> Slice<T>::REnd()
	{
		return {&First() - 1};
	}

	template<typename T>
	IteratorBackward<T const> Slice<T>::REnd() const
	{
		return {&First() - 1};
	}
}
