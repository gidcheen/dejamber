#pragma once

#include "iterators.hpp"
#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::collections
{
	struct Range
	{
		usize const start{};
		usize const end{};

		inline usize Count()
		{
			return end - start;
		}
	};

	template<typename T>
	struct Slice
	{
		usize const count{};
		T * const elements{};

		Slice(usize count, T * elements);

		T & operator[](usize index);
		T const & operator[](usize index) const;

		Slice<T> operator[](Range range);
		Slice<T const> operator[](Range range) const;

		b8 operator==(Slice<T> const & other) const;
		b8 operator!=(Slice<T> const & other) const;

		usize Count() const;
		b8 IsEmpty() const;

		T & First();
		T const & First() const;
		T & Last();
		T const & Last() const;

		IteratorForward<T> Begin();
		IteratorForward<T const> Begin() const;
		IteratorForward<T> End();
		IteratorForward<T const> End() const;

		IteratorBackward<T> RBegin();
		IteratorBackward<T const> RBegin() const;
		IteratorBackward<T> REnd();
		IteratorBackward<T const> REnd() const;
	};

	template<typename T>
	inline IteratorForward<T> begin(Slice<T> & list)
	{
		return list.Begin();
	}

	template<typename T>
	inline IteratorForward<T const> begin(Slice<T> const & list)
	{
		return list.Begin();
	}

	template<typename T>
	inline IteratorForward<T> end(Slice<T> & list)
	{
		return list.End();
	}

	template<typename T>
	inline IteratorForward<T const> end(Slice<T> const & list)
	{
		return list.End();
	}
}

#include "slice.impl.hpp"
