#pragma once

#include "list.hpp"
#include <heap/new.hpp>

namespace gid_tech::collections
{
	template<typename T>
	List<T>::List(ListInit init) :
		allocator{init.allocator},
		block_size{init.block_size},
		count{0},
		elements{nullptr}
	{
	}

	template<typename T>
	template<typename... TArgs>
	List<T>::List(usize count, TArgs &&... args) :
		List{ListInit{}, count, args...}
	{
	}

	template<typename T>
	template<typename... TArgs>
	List<T>::List(ListInit init, usize count, TArgs &&... args) :
		allocator{init.allocator},
		block_size{init.block_size},
		count{count},
		elements{Alloc()}
	{
		for (usize i{}; i < count; i++)
			new (&elements[i]) T{args...};
	}

	template<typename T>
	List<T>::List(Slice<T> slice) :
		List<T>{ListInit{}, slice}
	{
	}

	template<typename T>
	List<T>::List(ListInit init, Slice<T> slice) :
		allocator{init.allocator},
		block_size{init.block_size},
		count{slice.Count()},
		elements{Alloc()}
	{
		for (usize i{}; i < count; i++)
			new (&elements[i]) T{slice[i]};
	}

	template<typename T>
	template<typename TFunc>
	List<T>::List(usize count, ListInitFunc<TFunc> func) :
		List<T>{ListInit{}, count, func}
	{
	}

	template<typename T>
	template<typename TFunc>
	List<T>::List(ListInit init, usize count, ListInitFunc<TFunc> func) :
		allocator{init.allocator},
		block_size{init.block_size},
		count{count},
		elements{Alloc()}
	{
		for (usize i{}; i < count; i++)
			new (&elements[i]) T{func.func(i)};
	}

	template<typename T>
	List<T>::List(List<T> const & other) :
		allocator{other.allocator},
		block_size{other.block_size},
		count{other.count},
		elements{Alloc()}
	{
		for (usize i{}; i < count; i++)
			new (&elements[i]) T{other.elements[i]};
	}

	template<typename T>
	List<T>::List(List<T> && other) :
		allocator{other.allocator},
		block_size{other.block_size},
		count{other.count},
		elements{other.elements}
	{
		other.count = 0;
		other.elements = nullptr;
	}


	template<typename T>
	List<T>::~List()
	{
		for (usize i{}; i < count; i++)
			elements[i].~T();
		Dealloc(elements);
	}

	template<typename T>
	List<T> & List<T>::operator=(List<T> const & other)
	{
		for (usize i{}; i < count; i++)
			elements[i].~T();
		Dealloc(elements);

		count = other.count;

		elements = Alloc();
		for (usize i{}; i < count; i++)
			new (&elements[i]) T{other.elements[i]};
	}

	template<typename T>
	List<T> & List<T>::operator=(List<T> && other)
	{
		for (usize i{}; i < count; i++)
			elements[i].~T();
		Dealloc(elements);

		allocator = other.allocator;
		block_size = other.block_size;

		count = other.count;
		elements = other.elements;
		other.count = 0;
		other.elements = nullptr;
	}

	template<typename T>
	T & List<T>::operator[](usize index)
	{
		return elements[index];
	}

	template<typename T>
	T const & List<T>::operator[](usize index) const
	{
		return elements[index];
	}

	template<typename T>
	Slice<T> List<T>::operator[](Range range)
	{
		return {range.Count(), &elements[range.start]};
	}

	template<typename T>
	Slice<T const> List<T>::operator[](Range range) const
	{
		return {range.Count(), &elements[range.start]};
	}

	template<typename T>
	b8 List<T>::operator==(List<T> const & other) const
	{
		if (count == other.count)
		{
			for (usize i{}; i < count; i++)
			{
				if (elements[i] == other.elements[i])
					continue;
				return false;
			}
			return true;
		}
		else
		{
			return false;
		}
	}

	template<typename T>
	b8 List<T>::operator!=(List<T> const & other) const
	{
		if (count != other.count)
		{
			for (usize i{}; i < count; i++)
			{
				if (elements[i] != other.elements[i])
					continue;
				return false;
			}
			return true;
		}
		else
		{
			return false;
		}
	}

	template<typename T>
	usize List<T>::Count() const
	{
		return count;
	}

	template<typename T>
	b8 List<T>::IsEmpty() const
	{
		return count == 0;
	}

	template<typename T>
	T & List<T>::First()
	{
		return elements[0];
	}

	template<typename T>
	T const & List<T>::First() const
	{
		return elements[0];
	}

	template<typename T>
	T & List<T>::Last()
	{
		return elements[count - 1];
	}

	template<typename T>
	T const & List<T>::Last() const
	{
		return elements[count - 1];
	}

	template<typename T>
	IteratorForward<T> List<T>::Begin()
	{
		return {&First()};
	}

	template<typename T>
	IteratorForward<T const> List<T>::Begin() const
	{
		return {&First()};
	}

	template<typename T>
	IteratorForward<T> List<T>::End()
	{
		return {&Last() + 1};
	}

	template<typename T>
	IteratorForward<T const> List<T>::End() const
	{
		return {&Last() + 1};
	}

	template<typename T>
	IteratorBackward<T> List<T>::RBegin()
	{
		return {&Last()};
	}

	template<typename T>
	IteratorBackward<T const> List<T>::RBegin() const
	{
		return {&Last()};
	}

	template<typename T>
	IteratorBackward<T> List<T>::REnd()
	{
		return {&First() - 1};
	}

	template<typename T>
	IteratorBackward<T const> List<T>::REnd() const
	{
		return {&First() - 1};
	}

	template<typename T>
	template<typename... TArgs>
	T & List<T>::Add(TArgs &&... args)
	{
		return Add(ListIndex{count}, args...);
	}

	template<typename T>
	template<typename... TArgs>
	T & List<T>::Add(ListIndex index, TArgs &&... args)
	{
		auto block_count = GetBlockCount();
		count += 1;
		if (count > block_size * block_count)
		{
			auto old_elements = elements;
			elements = Alloc();
			for (usize i{}; i < count - 1; i++)
			{
				auto dst_index = i < index.index ? i : i + 1;
				new (&elements[dst_index]) T{static_cast<T &&>(old_elements[i])};
				elements[i].~T();
			}
			Dealloc(old_elements);
		}
		else if (index.index != count - 1)
		{
			for (usize i{count - 1}; i > index.index; i--)
			{
				new (&elements[i]) T{static_cast<T &&>(elements[i - 1])};
				elements[i - 1].~T();
			}
		}
		new (&elements[index.index]) T{args...};
		return elements[index.index];
	}

	template<typename T>
	void List<T>::Remove(ListIndex index)
	{
		for (usize i{index.index}; i < count; i++)
		{
			elements[i].~T();
			new (&elements[i]) T{static_cast<T &&>(elements[i + 1])};
		}
		elements[count - 1].~T();
		count -= 1;
	}

	template<typename T>
	void List<T>::Remove(T value)
	{
		ListIndex index{};
		for (usize i{}; i < count; i++)
		{
			if (elements[i] == value)
				break;
			index.index += 1;
		}
		Remove(index);
	}

	template<typename T>
	template<typename TPred>
	void List<T>::Remove(TPred pred)
	{
		ListIndex index{};
		for (usize i{}; i < count; i++)
		{
			if (pred(elements[i]))
				break;
			index.index += 1;
		}
		Remove(index);
	}

	template<typename T>
	usize List<T>::GetBlockCount()
	{
		auto full = count / block_size;
		auto partial = count % block_size > 0 ? 1 : 0;
		return full + partial;
	}

	template<typename T>
	T * List<T>::Alloc()
	{
		return count > 0 ? allocator.Alloc<T>(block_size * GetBlockCount()) : nullptr;
	}

	template<typename T>
	void List<T>::Dealloc(T * e)
	{
		if (e != nullptr)
			allocator.Dealloc(e);
	}
}
