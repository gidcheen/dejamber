#include "array.hpp"

#include <heap/new.hpp>

namespace gid_tech::collections
{
	template<typename T, usize TCount>
	T & Array<T, TCount>::operator[](usize index)
	{
		return elements[index];
	}

	template<typename T, usize TCount>
	T const & Array<T, TCount>::operator[](usize index) const
	{
		return elements[index];
	}

	template<typename T, usize TCount>
	Slice<T> Array<T, TCount>::operator[](Range range)
	{
		return {range.Count(), &elements[range.start]};
	}

	template<typename T, usize TCount>
	Slice<T const> Array<T, TCount>::operator[](Range range) const
	{
		return {range.Count(), &elements[range.start]};
	}

	template<typename T, usize TCount>
	b8 Array<T, TCount>::operator==(Array<T, TCount> const & other) const
	{
		b8 equal = true;
		for (usize i = 0; i < TCount; i++)
			equal &= elements[i] == other.elements[i];
		return equal;
	}

	template<typename T, usize TCount>
	b8 Array<T, TCount>::operator!=(Array<T, TCount> const & other) const
	{
		b8 not_equal = false;
		for (usize i = 0; i < TCount; i++)
			not_equal |= elements[i] != other.elements[i];
		return not_equal;
	}

	template<typename T, usize TCount>
	constexpr usize Array<T, TCount>::Count() const
	{
		return TCount;
	}

	template<typename T, usize TCount>
	constexpr b8 Array<T, TCount>::IsEmpty() const
	{
		return TCount == 0;
	}

	template<typename T, usize TCount>
	T & Array<T, TCount>::First()
	{
		return elements[0];
	}

	template<typename T, usize TCount>
	T const & Array<T, TCount>::First() const
	{
		return elements[0];
	}

	template<typename T, usize TCount>
	T & Array<T, TCount>::Last()
	{
		return elements[TCount - 1];
	}

	template<typename T, usize TCount>
	T const & Array<T, TCount>::Last() const
	{
		return elements[TCount - 1];
	}

	template<typename T, usize TCount>
	IteratorForward<T> Array<T, TCount>::Begin()
	{
		return {&First()};
	}

	template<typename T, usize TCount>
	IteratorForward<T const> Array<T, TCount>::Begin() const
	{
		return {&First()};
	}

	template<typename T, usize TCount>
	IteratorForward<T> Array<T, TCount>::End()
	{
		return {&Last() + 1};
	}

	template<typename T, usize TCount>
	IteratorForward<T const> Array<T, TCount>::End() const
	{
		return {&Last() + 1};
	}

	template<typename T, usize TCount>
	IteratorBackward<T> Array<T, TCount>::RBegin()
	{
		return {&Last()};
	}

	template<typename T, usize TCount>
	IteratorBackward<T const> Array<T, TCount>::RBegin() const
	{
		return {&Last()};
	}

	template<typename T, usize TCount>
	IteratorBackward<T> Array<T, TCount>::REnd()
	{
		return {&First() - 1};
	}

	template<typename T, usize TCount>
	IteratorBackward<T const> Array<T, TCount>::REnd() const
	{
		return {&First() - 1};
	}
}
