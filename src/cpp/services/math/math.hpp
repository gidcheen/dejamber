#pragma once

#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::math
{
	b8 IsInfinite(f32 x);
	b8 IsInfinite(f64 x);
	b8 IsNaN(f32 x);
	b8 IsNaN(f64 x);

	f32 Sin(f32 x);
	f64 Sin(f64 x);
	f32 Cos(f32 x);
	f64 Cos(f64 x);
	f32 Tan(f32 x);
	f64 Tan(f64 x);
	f32 Sinh(f32 x);
	f64 Sinh(f64 x);
	f32 Cosh(f32 x);
	f64 Cosh(f64 x);
	f32 Tanh(f32 x);
	f64 Tanh(f64 x);
	f32 Asin(f32 x);
	f64 Asin(f64 x);
	f32 Acos(f32 x);
	f64 Acos(f64 x);
	f32 Atan(f32 x);
	f64 Atan(f64 x);
	f32 Atan2(f32 y, f32 x);
	f64 Atan2(f64 y, f64 x);

	f32 Exp(f32 x);
	f64 Exp(f64 x);
	f32 Log(f32 x);
	f64 Log(f64 x);
	f32 Log10(f32 x);
	f64 Log10(f64 x);

	f32 Pow(f32 x, f32 y);
	f64 Pow(f64 x, f64 y);
	f32 Sqrt(f32 x);
	f64 Sqrt(f64 x);

	f32 Abs(f32 x);
	f64 Abs(f64 x);
	f32 Ceil(f32 x);
	f64 Ceil(f64 x);
	f32 Floor(f32 x);
	f64 Floor(f64 x);
	f32 Mod(f32 x, f32 y);
	f64 Mod(f64 x, f64 y);

	f32 CopySign(f32 x, f32 y);
	f64 CopySign(f64 x, f64 y);

	f32 DegToRad(f32 x);
	f64 DegToRad(f64 x);
	f32 RadToDeg(f32 x);
	f64 RadToDeg(f64 x);
}
