#pragma once

#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::math
{
	constexpr f64 infinity = +1e+300 * 1e+300;
	constexpr f64 negative_infinity = -infinity;
	constexpr f64 nan = __builtin_nanf("");

	constexpr f64 e = 2.718281828459045235360287471352662498;
	constexpr f64 log2e = 1.442695040888963407359924681001892137;
	constexpr f64 log10e = 0.434294481903251827651128918916605082;
	constexpr f64 ln2 = 0.693147180559945309417232121458176568;
	constexpr f64 ln10 = 2.302585092994045684017991454684364208;
	constexpr f64 pi = 3.141592653589793238462643383279502884;
	constexpr f64 sqrt2 = 1.414213562373095048801688724209698079;
}
