#include "json.hpp"

#include <string_utils/escaping.hpp>

#include "lexing.hpp"
#include "parsing.hpp"

using namespace std;
using namespace gid_tech::string_utils;
using namespace gid_tech::object_model;
using namespace gid_tech::object_model::json::lexing;
using namespace gid_tech::object_model::json::parsing;

namespace gid_tech::object_model::json
{
	optional<Object> JsonToObject(string const & json)
	{
		auto tokens = Lex(json);
		auto value = Parse(tokens);
		return value;
	}

	string ObjectToJson(Object const & value)
	{
		if (value.IsNull())
			return "null";
		if (auto b = value.AsBool())
			return to_string(b.value());
		if (auto i = value.AsInt())
			return to_string(i.value());
		if (auto f = value.AsFloat())
			return to_string(f.value());
		if (auto s = value.AsString())
			return "\"" + Escape(s.value()) + "\"";
		if (auto c = value.GetArrayCount())
		{
			string json = "[";
			for (usize i = 0; i < c.value(); i++)
				json += ObjectToJson(*value.GetArrayValue(i).value()) + (i != c.value() - 1 ? "," : "");
			json += "]";
			return json;
		}
		if (auto c = value.GetObjectCount())
		{
			string json = "{";
			for (usize i = 0; i < c.value(); i++)
				json += "\"" + *value.GetObjectName(i).value() + "\"" + ":" + ObjectToJson(*value.GetObjectValue(i).value()) + (i != c.value() - 1 ? "," : "");
			json += "}";
			return json;
		}
		exit(EXIT_FAILURE);
	}
}
