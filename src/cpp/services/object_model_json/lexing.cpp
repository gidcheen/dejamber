#include "lexing.hpp"

#include <optional>
#include <string_view>
#include <tuple>

#include <collections/array.hpp>
#include <string_utils/escaping.hpp>

using namespace std;
using namespace gid_tech::string_utils;
using namespace gid_tech::collections;

namespace gid_tech::object_model::json::lexing
{
	template<typename TC, typename TV>
	static bool Contains(TC const & collection, TV const & value)
	{
		b8 found = false;
		for (auto v : collection)
		{
			if (v == value)
			{
				found = true;
				break;
			}
		}
		return found;
	}

	static optional<tuple<Token, string_view>> LexNull(string_view const & json)
	{
		static string n = "null";
		if (json.size() >= n.size() && json.substr(0, n.size()) == n)
			return make_optional(make_tuple(Token(Null{}), json.substr(n.size())));
		else
			return nullopt;
	}

	static optional<tuple<Token, string_view>> LexBool(string_view const & json)
	{
		static string t = "true";
		static string f = "false";

		if (json.size() >= t.size() && json.substr(0, t.size()) == t)
			return make_optional(make_tuple(Token(Bool{true}), json.substr(t.size())));
		else if (json.size() >= f.size() && json.substr(0, f.size()) == t)
			return make_optional(make_tuple(Token(Bool{false}), json.substr(t.size())));
		else
			return nullopt;
	}

	static optional<tuple<Token, string_view>> LexNumber(string_view const & json)
	{
		static Array<char, 14> number_chars{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '+', '.', 'e'};

		usize size = 0;
		for (auto c : json)
		{
			// b8 found = false;
			// for (auto nc : number_chars)
			// {
			// 	if (nc == c)
			// 	{
			// 		found = true;
			// 		break;
			// 	}
			// }
			// if (found)
			// 	size += 1;
			// else
			// 	break;

			if (Contains(number_chars, c))
				size += 1;
			else
				break;
		}

		if (size != 0)
		{
			auto ret = json.substr(0, size);
			bool is_float = false;
			for (auto c : ret)
			{
				if (c == '.' || c == 'e')
				{
					is_float = true;
					break;
				}
			}

			if (is_float)
				return make_optional(make_tuple(Token(Float{stod(string(ret))}), json.substr(size)));
			else
				return make_optional(make_tuple(Token(Int{stoll(string(ret))}), json.substr(size)));
		}
		else
			return nullopt;
	}

	static optional<tuple<Token, string_view>> LexString(string_view const & json)
	{
		if (json.size() >= 2 && json[0] == '"')
		{
			bool escaped = false;
			usize size = 0;
			for (auto c : json.substr(1))
			{
				if (!escaped && c == '"')
					break;
				if (c == '\\')
					escaped = true;
				else
					escaped = false;
				size += 1;
			}
			auto ret = json.substr(1, size);
			return make_optional(make_tuple(Token(String{Unescape(string(ret))}), json.substr(size + 2)));
		}
		else
		{
			return nullopt;
		}
	}

	static optional<tuple<Token, string_view>> LexSyntax(string_view const & json)
	{
		if (json.size() >= 1)
		{
			switch (json[0])
			{
			case ',': return make_optional(make_tuple(Token(Comma{}), json.substr(1)));
			case ':': return make_optional(make_tuple(Token(Colon{}), json.substr(1)));
			case '[': return make_optional(make_tuple(Token(LeftBracket{}), json.substr(1)));
			case ']': return make_optional(make_tuple(Token(RightBracket{}), json.substr(1)));
			case '{': return make_optional(make_tuple(Token(LeftBrace{}), json.substr(1)));
			case '}': return make_optional(make_tuple(Token(RightBrace{}), json.substr(1)));
			default: return nullopt;
			}
		}
		else
			return nullopt;
	}

	vector<Token> Lex(string_view json)
	{
		static Array<char, 4> white_spaces{' ', '\t', '\n', '\r'};

		vector<Token> tokens;
		tokens.reserve(json.size() / 3);

		while (json.size() > 0)
		{
			auto c = json[0];
			if (Contains(white_spaces, c))
			{
				json = json.substr(1);
			}
			else
			{
				static Array<optional<tuple<Token, string_view>> (*)(string_view const & json), 5> lexers{
					LexNull,
					LexBool,
					LexNumber,
					LexString,
					LexSyntax,
				};

				for (auto lexer : lexers)
				{
					if (auto t = lexer(json))
					{
						auto [token, new_json] = t.value();
						tokens.emplace_back(move(token));
						json = new_json;
						break;
					}
				}
			}
		}
		return tokens;
	}
}
