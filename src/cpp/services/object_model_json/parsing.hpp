#pragma once

#include <optional>
#include <tuple>
#include <vector>

#include <object_model/object_model.hpp>

#include "lexing.hpp"

namespace gid_tech::object_model::json::parsing
{
	using namespace std;
	using namespace object_model;
	using namespace lexing;

	optional<Object> Parse(vector<Token> const & tokens);
}
