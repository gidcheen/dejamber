#include "parsing.hpp"

namespace gid_tech::object_model::json::parsing
{
	optional<tuple<Object, usize>> ParseRec(vector<Token> const & tokens, usize offset);

	optional<Object> Parse(vector<Token> const & tokens)
	{
		if (auto ret = ParseRec(tokens, 0))
		{
			auto [value, offset] = ret.value();
			if (offset == tokens.size())
				return make_optional(value);
			else
				return nullopt;
		}
		else
			return nullopt;
	}

	optional<tuple<Object, usize>> ParseRec(vector<Token> const & tokens, usize offset)
	{
		auto * token = &tokens[offset];
		if (auto n = get_if<Null>(token))
			return make_optional(make_tuple(Object(), offset + 1));
		else if (auto b = get_if<Bool>(token))
			return make_optional(make_tuple(Object(b->value), offset + 1));
		else if (auto i = get_if<Int>(token))
			return make_optional(make_tuple(Object(i->value), offset + 1));
		else if (auto f = get_if<Float>(token))
			return make_optional(make_tuple(Object(f->value), offset + 1));
		else if (auto s = get_if<String>(token))
			return make_optional(make_tuple(Object(s->value), offset + 1));
		else if (auto _ = get_if<LeftBracket>(token))
		{
			Object values;
			offset += 1;
			while (offset < tokens.size())
			{
				auto * next_token = &tokens[offset];
				if (auto _ = get_if<Comma>(next_token))
					offset += 1;
				else if (auto _ = get_if<RightBracket>(next_token))
					return make_optional(make_tuple(values, offset + 1));
				else if (auto next = ParseRec(tokens, offset))
				{
					auto [value, new_offset] = next.value();
					values.AddArrayValue(value);
					offset = new_offset;
				}
				else
					return nullopt;
			}
		}
		else if (auto _ = get_if<LeftBrace>(token))
		{
			Object values;
			offset += 1;
			while (offset < tokens.size())
			{
				auto * next_token = &tokens[offset];
				if (get_if<Comma>(next_token))
					offset += 1;
				else if (get_if<RightBrace>(next_token))
					return make_optional(make_tuple(values, offset + 1));
				else if (tokens.size() - offset > 3)
				{
					if (auto name = get_if<String>(&tokens[offset]))
					{
						if (get_if<Colon>(&tokens[offset + 1]))
						{
							if (auto next = ParseRec(tokens, offset + 2))
							{
								auto [value, new_offset] = next.value();
								values.AddObjectValue(name->value, value);
								offset = new_offset;
							}
							else
								return nullopt;
						}
						else
							return nullopt;
					}
					else
						return nullopt;
				}
				else
					return nullopt;
			}
		}
		return nullopt;
	}
}
