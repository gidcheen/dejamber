#ifdef WINDOWS

#include "window_manager.hpp"

#include <heap/new.hpp>

#include "implementation/window_manager.hpp"

namespace gid_tech::windowing
{
	WindowManager::WindowManager(string identifier) :
		implementation(*reinterpret_cast<Implementation const *>(implementation_data.elements))
	{
		static_assert(sizeof(WindowManager::implementation_data) >= sizeof(Implementation));
		new ((void *) &implementation) Implementation {
			.window_manager = win32::WindowManager(wstring(identifier.begin(), identifier.end())),
		};
	}

	WindowManager::~WindowManager()
	{
		implementation.~Implementation();
	}

	void * WindowManager::GetImplementationHandle() const
	{
		return implementation.window_manager.GetHinstance();
	}
}

#endif
