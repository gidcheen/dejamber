#pragma once

#ifdef WINDOWS

#include "../window_manager.hpp"
#include "implementation/window_manager.hpp"

namespace gid_tech::windowing
{
	struct WindowManager::Implementation
	{
		win32::WindowManager window_manager;
	};
}

#endif
