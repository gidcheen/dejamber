#ifdef LINUX

#include "window.hpp"
#include "window_manager.hpp"

namespace gid_tech::windowing::x11
{
	Window::Window(WindowManager const & window_manager, string title, Size size) :
		window_manager(window_manager),
		window(Window::CreateWindow(window_manager.GetDisplay(), title, size)),
		wm_delete_window(Window::CreateAtom(window_manager.GetDisplay(), window)),
		title(title),
		size(size)
	{
	}

	Window::~Window()
	{
		auto display = window_manager.GetDisplay();
		XUnmapWindow(display, window);
		XDestroyWindow(display, window);
	}

	::Window Window::GetWindow() const
	{
		return window;
	}

	Input const & Window::GetInput() const
	{
		return input;
	}

	string const & Window::GetTitle() const
	{
		return title;
	}

	Size const & Window::GetSize() const
	{
		return size;
	}

	void Window::Tick()
	{
		input.Step();

		auto display = window_manager.GetDisplay();
		XEvent event;
		if (XCheckTypedWindowEvent(display, window, ClientMessage, &event) && event.xclient.data.l[0] == wm_delete_window)
		{
			input.SetShouldClose();
		}
		else
		{
			while (XCheckWindowEvent(display, window, INT64_MAX, &event))
			{
				switch (event.type)
				{
				case MotionNotify:
				{
					input.SetMousePosition({event.xmotion.x, event.xmotion.y});
					break;
				}
				case ButtonPress:
				{
					if (event.xbutton.button <= 3)
					{
						auto mouse_button = Window::ToButton(event.xbutton.button);
						if (mouse_button.IsSome())
							input.SetMouseButton(mouse_button.Value(), Input::Type::Down);
					}
					break;
				}
				case ButtonRelease:
				{
					if (event.xbutton.button <= 3)
					{
						auto mouse_button = Window::ToButton(event.xbutton.button);
						if (mouse_button.IsSome())
							input.SetMouseButton(mouse_button.Value(), Input::Type::Up);
					}
					else if (event.xbutton.button == 4)
					{
						input.SetMouseWheel(input.GetMouseWheel() + 1);
					}
					else if (event.xbutton.button == 5)
					{
						input.SetMouseWheel(input.GetMouseWheel() - 1);
					}
					break;
				}
				case KeyPress:
				{
					auto key_code = event.xkey.keycode;
					auto keyboard_key = Window::ToKey(key_code);
					if (keyboard_key.IsSome())
						input.SetKeyboardKey(keyboard_key.Value(), Input::Type::Down);
					break;
				}
				case KeyRelease:
				{
					auto key_code = event.xkey.keycode;
					auto keyboard_key = Window::ToKey(key_code);
					if (keyboard_key.IsSome())
						input.SetKeyboardKey(keyboard_key.Value(), Input::Type::Up);
					break;
				}
				case ConfigureNotify:
				{
					size = Size{
						.width = static_cast<u32>(event.xconfigure.width),
						.height = static_cast<u32>(event.xconfigure.height),
					};
					break;
				}
				default:
					break;
				}
			}
		}
	}

	::Window Window::CreateWindow(::Display * display, string const & title, Size size)
	{
		auto screen = XDefaultScreen(display);
		auto root_window = XRootWindow(display, screen);

		auto window_attributes = XSetWindowAttributes{
			.background_pixel = XBlackPixel(display, screen),
			.event_mask =
				KeyPressMask | KeyReleaseMask |
				ButtonPressMask | ButtonReleaseMask |
				PointerMotionMask |
				StructureNotifyMask,
		};

		auto window = XCreateWindow(
			display,
			root_window,
			0, 0, size.width, size.height,
			0,
			0,
			InputOutput,
			nullptr,
			CWEventMask | CWBackPixel,
			&window_attributes);
		XMapWindow(display, window);
		XStoreName(display, window, title.c_str());

		return window;
	}

	::Atom Window::CreateAtom(::Display * display, ::Window window)
	{
		Atom wm_delete_window = XInternAtom(display, "WM_DELETE_WINDOW", false);
		XSetWMProtocols(display, window, &wm_delete_window, 1);
		return wm_delete_window;
	}

	Optional<Input::Keyboard::Key> Window::ToKey(::KeyCode key_code)
	{
		switch (key_code)
		{
		case 19:
			return Input::Keyboard::Key::Key0;
		case 10:
			return Input::Keyboard::Key::Key1;
		case 11:
			return Input::Keyboard::Key::Key2;
		case 12:
			return Input::Keyboard::Key::Key3;
		case 13:
			return Input::Keyboard::Key::Key4;
		case 14:
			return Input::Keyboard::Key::Key5;
		case 15:
			return Input::Keyboard::Key::Key6;
		case 16:
			return Input::Keyboard::Key::Key7;
		case 17:
			return Input::Keyboard::Key::Key8;
		case 18:
			return Input::Keyboard::Key::Key9;

		case 38:
			return Input::Keyboard::Key::A;
		case 56:
			return Input::Keyboard::Key::B;
		case 54:
			return Input::Keyboard::Key::C;
		case 40:
			return Input::Keyboard::Key::D;
		case 26:
			return Input::Keyboard::Key::E;
		case 41:
			return Input::Keyboard::Key::F;
		case 42:
			return Input::Keyboard::Key::G;
		case 43:
			return Input::Keyboard::Key::H;
		case 31:
			return Input::Keyboard::Key::I;
		case 44:
			return Input::Keyboard::Key::J;
		case 45:
			return Input::Keyboard::Key::K;
		case 46:
			return Input::Keyboard::Key::L;
		case 58:
			return Input::Keyboard::Key::M;
		case 57:
			return Input::Keyboard::Key::N;
		case 32:
			return Input::Keyboard::Key::O;
		case 33:
			return Input::Keyboard::Key::P;
		case 24:
			return Input::Keyboard::Key::Q;
		case 27:
			return Input::Keyboard::Key::R;
		case 39:
			return Input::Keyboard::Key::S;
		case 28:
			return Input::Keyboard::Key::T;
		case 30:
			return Input::Keyboard::Key::U;
		case 55:
			return Input::Keyboard::Key::V;
		case 25:
			return Input::Keyboard::Key::W;
		case 53:
			return Input::Keyboard::Key::X;
		case 29:
			return Input::Keyboard::Key::Y;
		case 52:
			return Input::Keyboard::Key::Z;

		case 65:
			return Input::Keyboard::Key::Space;
		case 49:
			return Input::Keyboard::Key::GraveAccent;
		case 20:
			return Input::Keyboard::Key::Minus;
		case 21:
			return Input::Keyboard::Key::Equal;
		case 34:
			return Input::Keyboard::Key::LeftBracket;
		case 35:
			return Input::Keyboard::Key::RightBracket;
		case 47:
			return Input::Keyboard::Key::Semicolon;
		case 48:
			return Input::Keyboard::Key::Apostrophe;
		case 51:
			return Input::Keyboard::Key::BackSlash;
		case 59:
			return Input::Keyboard::Key::Comma;
		case 60:
			return Input::Keyboard::Key::Period;
		case 61:
			return Input::Keyboard::Key::Slash;

		case 50:
		case 62:
			return Input::Keyboard::Key::Shift;
		case 37:
		case 105:
			return Input::Keyboard::Key::Control;
		case 64:
		case 108:
			return Input::Keyboard::Key::Alt;

		case 23:
			return Input::Keyboard::Key::Tab;
		case 66:
			return Input::Keyboard::Key::CapsLock;
		case 22:
			return Input::Keyboard::Key::BackSpace;
		case 36:
			return Input::Keyboard::Key::Enter;

		case 9:
			return Input::Keyboard::Key::Escape;

		case 67:
			return Input::Keyboard::Key::F1;
		case 68:
			return Input::Keyboard::Key::F2;
		case 69:
			return Input::Keyboard::Key::F3;
		case 70:
			return Input::Keyboard::Key::F4;
		case 71:
			return Input::Keyboard::Key::F5;
		case 72:
			return Input::Keyboard::Key::F6;
		case 73:
			return Input::Keyboard::Key::F7;
		case 74:
			return Input::Keyboard::Key::F8;
		case 75:
			return Input::Keyboard::Key::F9;
		case 76:
			return Input::Keyboard::Key::F10;
		case 95:
			return Input::Keyboard::Key::F11;
		case 96:
			return Input::Keyboard::Key::F12;

		case 118:
			return Input::Keyboard::Key::Insert;
		case 119:
			return Input::Keyboard::Key::Delete;
		case 110:
			return Input::Keyboard::Key::Home;
		case 115:
			return Input::Keyboard::Key::End;
		case 112:
			return Input::Keyboard::Key::PageUp;
		case 117:
			return Input::Keyboard::Key::PageDown;

		case 113:
			return Input::Keyboard::Key::Left;
		case 114:
			return Input::Keyboard::Key::Right;
		case 111:
			return Input::Keyboard::Key::Up;
		case 116:
			return Input::Keyboard::Key::Down;

		case 77:
			return Input::Keyboard::Key::NumLock;
		case 90:
			return Input::Keyboard::Key::Num0;
		case 87:
			return Input::Keyboard::Key::Num1;
		case 88:
			return Input::Keyboard::Key::Num2;
		case 89:
			return Input::Keyboard::Key::Num3;
		case 83:
			return Input::Keyboard::Key::Num4;
		case 84:
			return Input::Keyboard::Key::Num5;
		case 85:
			return Input::Keyboard::Key::Num6;
		case 79:
			return Input::Keyboard::Key::Num7;
		case 80:
			return Input::Keyboard::Key::Num8;
		case 81:
			return Input::Keyboard::Key::Num9;
		case 106:
			return Input::Keyboard::Key::NumDivide;
		case 63:
			return Input::Keyboard::Key::NumMultiply;
		case 82:
			return Input::Keyboard::Key::NumSubtract;
		case 86:
			return Input::Keyboard::Key::NumAdd;
		case 91:
			return Input::Keyboard::Key::NumDecimal;

		default:
			return OptionalNone;
		}
	}

	Optional<Input::Mouse::Button> Window::ToButton(u32 button)
	{
		switch (button)
		{
		case 1:
			return Input::Mouse::Button::Left;
		case 3:
			return Input::Mouse::Button::Right;
		case 2:
			return Input::Mouse::Button::Middle;
		default:
			return OptionalNone;
		}
	}
}

#endif
