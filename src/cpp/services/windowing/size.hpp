#pragma once

#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::windowing
{
	struct Size
	{
		u32 width{};
		u32 height{};
	};
}
