#pragma once

#include <string>

#include <collections/array.hpp>
#include <types/types.hpp>

using namespace std;
using namespace gid_tech::types;
using namespace gid_tech::collections;

namespace gid_tech::windowing
{
	class Input
	{
	public:
		enum class Type : usize
		{
			Up,
			Down,
		};

		struct Mouse
		{
			enum class Button : usize
			{
				Left,
				Right,
				Middle,
			};
			struct Position
			{
				i64 x{};
				i64 y{};
			};

			Position position{};
			Array<Type, (usize) Button::Middle + 1> buttons{};
			i64 wheel{};
		};

		struct Keyboard
		{
			enum class Key : usize
			{
				Key1,
				Key2,
				Key3,
				Key4,
				Key5,
				Key6,
				Key7,
				Key8,
				Key9,
				Key0,

				A,
				B,
				C,
				D,
				E,
				F,
				G,
				H,
				I,
				J,
				K,
				L,
				M,
				N,
				O,
				P,
				Q,
				R,
				S,
				T,
				U,
				V,
				W,
				X,
				Y,
				Z,

				Space,
				GraveAccent,
				Minus,
				Equal,
				LeftBracket,
				RightBracket,
				Semicolon,
				Apostrophe,
				BackSlash,
				Comma,
				Period,
				Slash,

				Shift,
				Control,
				Alt,
				Super,

				Tab,
				CapsLock,
				BackSpace,
				Enter,

				Escape,

				F1,
				F2,
				F3,
				F4,
				F5,
				F6,
				F7,
				F8,
				F9,
				F10,
				F11,
				F12,

				Insert,
				Delete,
				Home,
				End,
				PageUp,
				PageDown,

				Left,
				Right,
				Up,
				Down,

				NumLock,
				Num0,
				Num1,
				Num2,
				Num3,
				Num4,
				Num5,
				Num6,
				Num7,
				Num8,
				Num9,
				NumDivide,
				NumMultiply,
				NumSubtract,
				NumAdd,
				NumDecimal,
			};

			Array<Type, (usize) Key::NumDecimal + 1> keys{};
			string text{};
		};

	private:
		Mouse mouse_last;
		Mouse mouse_current;

		Keyboard keyboard_last;
		Keyboard keyboard_current;

		b8 should_close{};

	public:
		Mouse::Position GetMousePosition() const;
		Mouse::Position GetMousePositionDelta() const;
		b8 GetMouseButton(Mouse::Button button) const;
		b8 GetMouseButtonUp(Mouse::Button button) const;
		b8 GetMouseButtonDown(Mouse::Button button) const;
		i64 GetMouseWheel() const;
		i64 GetMouseWheelDelta() const;

		b8 GetKeyboardKey(Keyboard::Key key) const;
		b8 GetKeyboardKeyUp(Keyboard::Key key) const;
		b8 GetKeyboardKeyDown(Keyboard::Key key) const;
		string const & GetKeyboardText() const;

		b8 ShouldClose() const;

		void SetMousePosition(Mouse::Position position);
		void SetMouseButton(Mouse::Button button, Type type);
		void SetMouseWheel(i64 value);

		void SetKeyboardKey(Keyboard::Key key, Type type);
		void SetKeyboardText(string const & text);

		void SetShouldClose();

		void Step();
	};
}
