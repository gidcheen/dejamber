#include "basic.hpp"

namespace gid_tech::parsing
{
	b8 IsDigit(c8 c)
	{
		return c >= '0' && c <= '9';
	};
	b8 IsSpace(c8 c)
	{
		return c == ' ';
	};

	isize GetSign(c8 const ** str)
	{
		isize sign{1};
		if ((*str)[0] == '+')
		{
			*str += 1;
		}
		else if ((*str)[0] == '-')
		{
			sign = -1;
			*str += 1;
		}
		return sign;
	}

	b8 ToB8(c8 const * str)
	{
		while (IsSpace(str[0]))
			str += 1;

		return (str[0] == 't' && str[1] == 'r' && str[2] == 'u' && str[3] == 'e') ||
			(str[0] != '0' && IsDigit(str[0]));
	}

	u64 ToU64(c8 const * str)
	{
		while (IsSpace(str[0]))
			str += 1;

		isize sign = GetSign(&str);
		u64 ret{};

		while (IsDigit(str[0]))
		{
			ret = 10 * ret - (str[0] - '0');
			str += 1;
		}
		return ret * -sign;
	}

	f64 ToF64(c8 const * str)
	{
		while (IsSpace(str[0]))
			str += 1;

		isize sign = GetSign(&str);
		f64 ret{};
		isize e{};

		while (str[0] != '\0' && IsDigit(str[0]))
		{
			ret = ret * 10.0 + (str[0] - '0');
			str += 1;
		}
		if (str[0] == '.')
		{
			str += 1;
			while (str[0] != '\0' && IsDigit(str[0]))
			{
				ret = ret * 10.0 + (str[0] - '0');
				e = e - 1;
				str += 1;
			}
		}
		if (str[0] == 'e' || str[0] == 'E')
		{
			str += 1;

			isize e_i = 0;
			isize e_sign = GetSign(&str);

			while (IsDigit(str[0]))
			{
				e_i = e_i * 10 + (str[0] - '0');
				str += 1;
			}
			e += e_i * e_sign;
		}
		while (e > 0)
		{
			ret *= 10.0;
			e--;
		}
		while (e < 0)
		{
			ret *= 0.1;
			e++;
		}
		return ret * sign;
	}
}
