#pragma once

#include <string>
#include <tuple>
#include <types/types.hpp>
#include <vector>

using namespace std;
using namespace gid_tech::types;

namespace gid_tech::image_decoding
{
	struct Extent
	{
		usize width;
		usize height;
	};
	struct Pixel
	{
		u8 r, g, b, a;
	};

	tuple<vector<Pixel>, Extent> Decode(vector<u8> const & data);

	template<typename T>
	tuple<vector<T>, Extent> DecodeTo(vector<u8> const & data);


	template<typename TP>
	inline tuple<vector<TP>, Extent> DecodeTo(vector<u8> const & data)
	{
		static_assert(sizeof(TP) == sizeof(Pixel));
		auto decoded = Decode(data);
		return *reinterpret_cast<tuple<vector<TP>, Extent> *>(&decoded);
	}
}
