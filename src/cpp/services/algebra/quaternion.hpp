#pragma once

#include <collections/array.hpp>
#include <types/traits.hpp>

#include "vector.hpp"

using namespace std;
using namespace gid_tech::types;

namespace gid_tech::algebra
{
	template<typename T>
	struct Quaternion
	{
		static_assert(IsFloating<T>::value);

		union
		{
			struct
			{
				T x;
				T y;
				T z;
				T w;
			};
			Vector<T, 3> vector3;
			Vector<T, 4> vector4;
			Array<T, 4> elements;
			T elements_raw[4]{0, 0, 0, 1};
		};

		Quaternion();
		Quaternion(Quaternion<T> const & other);
		Quaternion(T x, T y, T z, T w);

		Quaternion<T> & operator=(Quaternion<T> const &) = default;

		Quaternion<T> & operator+=(Quaternion<T> const & other);
		Quaternion<T> & operator-=(Quaternion<T> const & other);
		Quaternion<T> operator+(Quaternion<T> const & other);
		Quaternion<T> operator-(Quaternion<T> const & other);

		Quaternion<T> & operator*=(T other);
		Quaternion<T> & operator/=(T other);
		Quaternion<T> operator*(T other) const;
		Quaternion<T> operator/(T other) const;

		Quaternion<T> & operator*=(Quaternion<T> const & other);
		Quaternion<T> & operator/=(Quaternion<T> const & other);
		Quaternion<T> operator*(Quaternion<T> const & other) const;
		Quaternion<T> operator/(Quaternion<T> const & other) const;

		Vector<T, 3> operator*(Vector<T, 3> const & other) const;

		b8 operator==(Quaternion<T> const & other) const;
		b8 operator!=(Quaternion<T> const & other) const;

		Quaternion<T> & Invert();
		Quaternion<T> & Inverted() const;

		T LengthSquared() const;
		T Length() const;

		Quaternion<T> & Normalize();
		Quaternion<T> Normalized() const;

		static Quaternion<T> Identity();
		static Quaternion<T> FromAxisAngle(Vector<T, 3> const & n, T a);
	};
}

#include "quaternion.impl.hpp"
