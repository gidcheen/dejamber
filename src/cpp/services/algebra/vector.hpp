#pragma once

#include "matrix.hpp"

namespace gid_tech::algebra
{
	template<typename T, usize X>
	using Vector = Matrix<T, X, 1>;
}
