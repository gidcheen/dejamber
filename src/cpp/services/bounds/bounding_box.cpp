#include "bounding_box.hpp"

namespace gid_tech::bounds
{
	BoundingBox::BoundingBox(Vector<f32, 3> min, Vector<f32, 3> max) :
		min(min), max(max)
	{
	}

	Vector<f32, 3> BoundingBox::GetSize() const
	{
		return max - min;
	}
	Vector<f32, 3> BoundingBox::GetExtend() const
	{
		return GetSize() * 0.5;
	}
	Vector<f32, 3> BoundingBox::GetCenter() const
	{
		return min + GetExtend();
	}

	f32 BoundingBox::GetRadius() const
	{
		return GetExtend().Length();
	}
	f32 BoundingBox::GetRadiusSquared() const
	{
		return GetExtend().LengthSquared();
	}

	BoundingBox & BoundingBox::Add(BoundingBox const & other)
	{
		min = min.Min(other.min);
		max = max.Max(other.max);
		return *this;
	}

	b8 BoundingBox::Inside(Vector<f32, 3> point) const
	{
		b8 x = point.x >= min.x && point.x <= max.x;
		b8 y = point.y >= min.y && point.y <= max.y;
		b8 z = point.z >= min.z && point.z <= max.z;
		return x && y && z;
	}

	b8 BoundingBox::Overlaps(const BoundingBox & a, const BoundingBox & b)
	{
		b8 x = a.min.x <= b.max.x && a.max.x >= b.min.x;
		b8 z = a.min.y <= b.max.y && a.max.y >= b.min.y;
		b8 y = a.min.z <= b.max.z && a.max.z >= b.min.z;
		return x && y && z;
	}

	b8 BoundingBox::Overlaps(BoundingBox const & other) const
	{
		return Overlaps(*this, other);
	}

	BoundingBox BoundingBox::Transformed(Matrix<f32, 4, 4> const & trans) const
	{
		return BoundingBox(*this).Transform(trans);
	}

	BoundingBox & BoundingBox::Transform(Matrix<f32, 4, 4> const & trans)
	{
		Array<Vector<f32, 3>, 8> transformed_corner = GetCorners();
		for (int i = 0; i < 8; i++)
		{
			auto transformed = trans * Vector<f32, 4>(transformed_corner[i].x, transformed_corner[i].y, transformed_corner[i].z, 1);
			transformed_corner[i] = Vector<f32, 3>(transformed.x, transformed.y, transformed.z);
		}
		min = transformed_corner[0];
		max = transformed_corner[0];
		for (int i = 1; i < 8; i++)
		{
			min = min.Min(transformed_corner[i]);
			max = max.Max(transformed_corner[i]);
		}
		return *this;
	}

	Array<Vector<f32, 3>, 8> BoundingBox::GetCorners()
	{
		Array<Vector<f32, 3>, 8> corners;
		auto center = GetCenter();
		auto extent = GetExtend();
		for (int i = 0; i < 8; i++)
		{
			Vector<f32, 3> corner_mult{
				static_cast<f32>((i & (1 << 2)) == 0 ? 1 : -1),
				static_cast<f32>((i & (1 << 1)) == 0 ? 1 : -1),
				static_cast<f32>((i & (1 << 0)) == 0 ? 1 : -1),
			};

			corners[i] = center + extent.ComponentMultiplied(corner_mult);
		}
		return corners;
	}
}
