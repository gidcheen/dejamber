#pragma once

#include <string>

namespace gid_tech::string_utils
{
	using std::string;

	string Escape(string const & s);
	string Unescape(string const & s);
}
