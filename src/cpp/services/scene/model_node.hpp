#pragma once

#include <memory>
#include <vector>

#include <bounds/bounding_box.hpp>

#include <rendering/resources/image.hpp>
#include <rendering/resources/material.hpp>
#include <rendering/resources/mesh.hpp>
#include <rendering/resources/pipeline.hpp>
#include <rendering/scene/surface.hpp>

#include "node.hpp"

namespace gid_tech::scene
{
	using namespace std;
	using namespace rendering;
	using namespace bounds;

	class Scene;

	class ModelNode : public Node
	{
		friend class Scene;

	public:
		struct SurfaceData
		{
			string name;
			shared_ptr<rendering::Pipeline> pipeline;
			shared_ptr<rendering::Mesh> mesh;
			shared_ptr<rendering::Material> material;
			vector<shared_ptr<rendering::Image>> images;
		};

	private:
		unique_ptr<rendering::Surface> surface;
		vector<SurfaceData> surfaces;

		Optional<BoundingBox> bounding_box;
		bool needs_upload{true};

	public:
		ModelNode(string name, Transform transform, vector<SurfaceData> surfaces);
		ModelNode(string name, Transform transform, Scene * scene, vector<SurfaceData> surfaces);
		ModelNode(string name, Transform transform, Node * parent, vector<SurfaceData> surfaces);
		ModelNode(string name, Transform transform, Optional<variant<Scene *, Node *>> scene_or_parent, vector<SurfaceData> surfaces);
		virtual ~ModelNode();


		vector<SurfaceData> & GetSurfaces();
		vector<SurfaceData> const & GetSurfaces() const;
		void SetSurfaces(vector<SurfaceData> surfaces);

		BoundingBox const & GetBoundingBox();

		Node * Copy(function<void(Node *)> node_alloc_callback, Optional<variant<Scene *, Node *>> scene_or_parent) override;

	protected:
		void TransformChanged() override;
		void SceneChanged() override;

	private:
		data::Surface GetSurfaceData();
		void Upload();
	};
}
