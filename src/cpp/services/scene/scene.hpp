#pragma once

#include <memory>
#include <string>
#include <vector>

#include <aggregates/optional.hpp>
#include <rendering/drawing/frame_drawer.hpp>
#include <rendering/render_manager.hpp>
#include <rendering/scene/scene.hpp>

using namespace std;
using namespace gid_tech::rendering;
using namespace gid_tech::aggregates;

namespace gid_tech::scene
{
	class Node;
	class LightNode;
	class ModelNode;
	class CameraNode;

	class Scene
	{
		friend class Node;
		friend class LightNode;
		friend class ModelNode;
		friend class CameraNode;

	private:
		RenderManager & render_manager;

		vector<Node *> nodes;
		vector<Node *> root_nodes;

		vector<LightNode *> light_nodes;
		vector<ModelNode *> model_nodes;
		vector<CameraNode *> camera_nodes;

		rendering::SceneManager scene;
		FrameDrawer frame_drawer;

	public:
		Scene(RenderManager & render_manager);

		Scene(Scene const &) = delete;
		Scene & operator=(Scene const &) = delete;


		vector<Node *> const & GetRootNodes() const;
		Optional<Node *> FindRootNode(string const & name);
		Optional<Node const *> FindRootNode(string const & name) const;

		void Tick();

	private:
		void AddNode(Node * node);
		void RemoveNode(Node * node);

		static bool SortDraw(FrameDraw const & a, FrameDraw const & b, Camera const * camera);
	};
}
