#include "camera_node.hpp"

#include <algebra/transform.hpp>

#include "scene.hpp"

namespace gid_tech::scene
{
	CameraNode::CameraNode(string name, Transform transform, Config config, shared_ptr<Frame> frame) :
		CameraNode(move(name), transform, OptionalNone, config, frame)
	{
	}

	CameraNode::CameraNode(string name, Transform transform, Scene * scene, Config config, shared_ptr<Frame> frame) :
		CameraNode(move(name), transform, Optional<variant<Scene *, Node *>>(scene), config, frame)
	{
	}

	CameraNode::CameraNode(string name, Transform transform, Node * parent, Config config, shared_ptr<Frame> frame) :
		CameraNode(move(name), transform, Optional<variant<Scene *, Node *>>(parent), config, frame)
	{
	}

	CameraNode::CameraNode(string name, Transform transform, Optional<variant<Scene *, Node *>> scene_or_parent, Config config, shared_ptr<Frame> frame) :
		Node(move(name), transform, OptionalNone),
		config(config),
		camera(nullptr),
		frame(move(frame))
	{
		SetSceneOrParent(scene_or_parent);
	}

	CameraNode::~CameraNode()
	{
		SetSceneOrParent(OptionalNone);
	}

	Frame const & CameraNode::GetFrame() const
	{
		return *frame.get();
	}

	void CameraNode::SetFrame(shared_ptr<Frame> frame)
	{
		this->frame = frame;
	}

	CameraNode::Config const & CameraNode::GetConfig()
	{
		return config;
	}

	void CameraNode::SetConfig(Config config)
	{
		this->config = config;
		local_to_projection = OptionalNone;
		frustum = OptionalNone;
		needs_upload = true;
	}

	Frustum const & CameraNode::GetFrustum()
	{
		if (!frustum.IsSome())
			frustum = Optional<Frustum>(GetLocalToProjection() * GetWorldToLocal());
		return frustum.Value();
	}

	Matrix<f32, 4, 4> CameraNode::GetLocalToProjection()
	{
		if (!local_to_projection.IsSome())
			local_to_projection = Optional<Matrix<f32, 4, 4>>(algebra::Transform<f32>::Perspective(config.fov, config.apsect, config.near, config.far));
		return local_to_projection.Value();
	}

	Node * CameraNode::Copy(function<void(Node *)> node_alloc_callback, Optional<variant<Scene *, Node *>> scene_or_parent)
	{
		auto node = new CameraNode(GetName(), GetTransform(), scene_or_parent, config, frame);
		node_alloc_callback(node);
		for (usize i = 0; i < GetChildCount(); i++)
			GetChild(i)->Copy(node_alloc_callback, Optional<variant<Scene *, Node *>>(static_cast<Node *>(node)));
		return node;
	}

	void CameraNode::TransformChanged()
	{
		Node::TransformChanged();
		local_to_projection = OptionalNone;
		frustum = OptionalNone;
		needs_upload = true;
	}

	void CameraNode::SceneChanged()
	{
		Node::SceneChanged();
		camera.reset();
	}

	data::Camera CameraNode::GetCameraData()
	{
		return data::Camera{
			.world_to_local = GetWorldToLocal(),
			.local_to_world = GetLocalToWorld(),
			.local_to_projection = GetLocalToProjection(),
		};
	}

	void CameraNode::Upload()
	{
		if (camera == nullptr)
			camera = make_unique<Camera>(GetScene().Value()->scene, GetCameraData());
		else if (needs_upload)
			camera->Set(GetCameraData());
		needs_upload = false;
	}
}
