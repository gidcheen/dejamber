#include "scene.hpp"

#include <utility>

#include <algebra/transform.hpp>

#include "camera_node.hpp"
#include "light_node.hpp"
#include "model_node.hpp"

#include <iostream>

namespace gid_tech::scene
{
	Scene::Scene(RenderManager & render_manager) :
		render_manager(render_manager),
		scene(render_manager),
		frame_drawer(render_manager)
	{
	}

	vector<Node *> const & Scene::GetRootNodes() const
	{
		return root_nodes;
	}

	Optional<Node *> Scene::FindRootNode(string const & name)
	{
		for (auto n : root_nodes)
		{
			if (n->name == name)
				return  Optional<Node *>(n);
		}
		return OptionalNone;
	}

	Optional<Node const *> Scene::FindRootNode(string const & name) const
	{
		for (auto n : root_nodes)
		{
			if (n->name == name)
				return Optional<Node const *>(n);
		}
		return OptionalNone;
	}

	void Scene::Tick()
	{
		for (auto * camera_node : camera_nodes)
		{
			camera_node->Upload();

			auto & frustum = camera_node->GetFrustum();
			vector<FrameDraw> draws;
			draws.reserve(128);

			for (auto model_node : model_nodes)
			{
				auto & bounding_box = model_node->GetBoundingBox();
				if (frustum.Contains(bounding_box))
				{
					model_node->Upload();

					usize surfaces_added = 0;
					for (auto & model_node_surface : model_node->surfaces)
					{
						vector<Light const *> lights;

						for (auto * light_node : light_nodes)
						{
							auto & light_bounding_box = light_node->GetBoundingBox();
							if (bounding_box.Overlaps(light_bounding_box))
							{
								light_node->Upload();
								lights.emplace_back(light_node->light.get());
								if (lights.size() > 10)
									break;
							}
						}

						// cout << lights.size() << endl;

						vector<Image const *> images;
						images.reserve(model_node_surface.images.size());
						for (auto & i : model_node_surface.images)
							images.emplace_back(i.get());

						FrameDraw draw{
							.lights = move(lights),
							.surface = model_node->surface.get(),
							.camera = camera_node->camera.get(),

							.pipeline = model_node_surface.pipeline.get(),
							.mesh = model_node_surface.mesh.get(),
							.material = model_node_surface.material.get(),
							.images = move(images),
						};
						draws.emplace_back(move(draw));
						surfaces_added += 1;
					}
				}
			}

			sort(draws.begin(), draws.end(), [&](FrameDraw const & a, FrameDraw const & b) {
				return SortDraw(a, b, camera_node->camera.get());
			});
			frame_drawer.Draw(&this->scene, draws, camera_node->frame.get());

			// cout << draws.size() << endl;
		}
	}

	void Scene::AddNode(Node * node)
	{
		nodes.emplace_back(node);

		if (node->GetParent().IsNone())
			root_nodes.emplace_back(node);

		if (auto light_node = dynamic_cast<LightNode *>(node))
			light_nodes.emplace_back(light_node);
		if (auto model_node = dynamic_cast<ModelNode *>(node))
			model_nodes.emplace_back(model_node);
		if (auto camera_node = dynamic_cast<CameraNode *>(node))
			camera_nodes.emplace_back(camera_node);

		for (auto child : node->children)
			AddNode(child);
	}

	void Scene::RemoveNode(Node * node)
	{
		nodes.erase(find(nodes.begin(), nodes.end(), node));

		if (node->GetParent().IsNone())
			root_nodes.erase(find(root_nodes.begin(), root_nodes.end(), node));

		if (auto light_node = dynamic_cast<LightNode const *>(node))
			light_nodes.erase(find(light_nodes.begin(), light_nodes.end(), light_node));
		if (auto model_node = dynamic_cast<ModelNode const *>(node))
			model_nodes.erase(find(model_nodes.begin(), model_nodes.end(), model_node));
		if (auto camera_node = dynamic_cast<CameraNode const *>(node))
			camera_nodes.erase(find(camera_nodes.begin(), camera_nodes.end(), camera_node));

		for (auto child : node->children)
			RemoveNode(child);
	}

	bool Scene::SortDraw(FrameDraw const & a, FrameDraw const & b, Camera const * camera)
	{
		if (a.pipeline->IsTransparent() || b.pipeline->IsTransparent())
		{
			return !a.pipeline->IsTransparent();
		}
		else if (a.pipeline->IsTransparent() && b.pipeline->IsTransparent())
		{
			Vector<f32, 3> a_pos = Transform<f32>::MatrixToPosition(a.surface->Get().local_to_world);
			Vector<f32, 3> b_pos = Transform<f32>::MatrixToPosition(b.surface->Get().local_to_world);

			Vector<f32, 3> a_camera_pos = camera->Get().world_to_local * Vector<f32, 4>{a_pos.x, a_pos.y, a_pos.z, 1};
			Vector<f32, 3> b_camera_pos = camera->Get().world_to_local * Vector<f32, 4>{b_pos.x, b_pos.y, b_pos.z, 1};

			return a_camera_pos.z > b_camera_pos.z;
		}
		else
		{
			return a.pipeline < b.pipeline;
		}
	}
}
