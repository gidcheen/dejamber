#include "model_node.hpp"

#include "scene.hpp"

namespace gid_tech::scene
{
	ModelNode::ModelNode(string name, Transform transform, vector<SurfaceData> surfaces) :
		ModelNode(move(name), transform, OptionalNone, move(surfaces))
	{
	}

	ModelNode::ModelNode(string name, Transform transform, Scene * scene, vector<SurfaceData> surfaces) :
		ModelNode(move(name), transform, Optional<variant<Scene *, Node *>>(scene), move(surfaces))
	{
	}

	ModelNode::ModelNode(string name, Transform transform, Node * parent, vector<SurfaceData> surfaces) :
		ModelNode(move(name), transform, Optional<variant<Scene *, Node *>>(parent), move(surfaces))
	{
	}

	ModelNode::ModelNode(string name, Transform transform, Optional<variant<Scene *, Node *>> scene_or_parent, vector<SurfaceData> surfaces) :
		Node(move(name), transform, OptionalNone),
		surface(nullptr),
		surfaces(move(surfaces))
	{
		SetSceneOrParent(scene_or_parent);
	}

	ModelNode::~ModelNode()
	{
		SetSceneOrParent(OptionalNone);
	}

	vector<ModelNode::SurfaceData> & ModelNode::GetSurfaces()
	{
		return surfaces;
	}

	vector<ModelNode::SurfaceData> const & ModelNode::GetSurfaces() const
	{
		return surfaces;
	}

	void ModelNode::SetSurfaces(vector<SurfaceData> surfaces)
	{
		this->surfaces = move(surfaces);
		bounding_box = OptionalNone;
	}

	BoundingBox const & ModelNode::GetBoundingBox()
	{
		if (bounding_box.IsNone())
		{
			auto & local_to_world = GetLocalToWorld();
			bounding_box = Optional<BoundingBox>(BoundingBox(surfaces[0].mesh->GetBounds().min, surfaces[0].mesh->GetBounds().max));
			for (usize i = 1; i < surfaces.size(); i++)
				bounding_box->Add(BoundingBox(surfaces[i].mesh->GetBounds().min, surfaces[i].mesh->GetBounds().max));
			bounding_box->Transform(local_to_world);
		}
		return bounding_box.Value();
	}

	Node * ModelNode::Copy(function<void(Node *)> node_alloc_callback, Optional<variant<Scene *, Node *>> scene_or_parent)
	{
		auto node = new ModelNode(GetName(), GetTransform(), scene_or_parent, surfaces);
		node_alloc_callback(node);
		for (usize i = 0; i < GetChildCount(); i++)
			GetChild(i)->Copy(node_alloc_callback, Optional<variant<Scene *, Node *>>(static_cast<Node *>(node)));
		return node;
	}

	void ModelNode::TransformChanged()
	{
		Node::TransformChanged();
		bounding_box = OptionalNone;
		needs_upload = true;
	}

	void ModelNode::SceneChanged()
	{
		Node::SceneChanged();
		surface.reset();
	}

	data::Surface ModelNode::GetSurfaceData()
	{
		return data::Surface{
			.local_to_world = GetLocalToWorld(),
		};
	}

	void ModelNode::Upload()
	{
		if (surface == nullptr)
			surface = make_unique<Surface>(GetScene().Value()->scene, GetSurfaceData());
		else if (needs_upload)
			surface->Set(GetSurfaceData());
		needs_upload = false;
	}
}
