#include "frame_drawer.hpp"

#include <types/types.hpp>

#include "../../vulkan/render_manager.hpp"
#include "../frame_drawer.hpp"

namespace gid_tech::rendering
{
	FrameDrawer::FrameDrawer(RenderManager & render_manager) :
		FrameDrawerImplementation(render_manager.device, render_manager.frame_config)
	{
	}

	FrameDrawer::~FrameDrawer()
	{
	}

	void FrameDrawer::Draw(SceneManager const * scene, vector<FrameDraw> const & draws, Frame const * frame)
	{
		auto & implementation_draws = reinterpret_cast<vector<implementation::FrameDraw> const &>(draws);
		FrameDrawerImplementation::Draw(scene, implementation_draws, frame);
	}
}

namespace gid_tech::rendering::implementation
{
	using std::array;
	using std::make_unique;
	using std::unique_ptr;

	struct ObjectIndices
	{
		u16 light_count;
		u16 lights[11];
		u16 surface;
		u16 camera;
	};

	FrameDrawer::FrameDrawer(Device & device, FrameConfig const & frame_config) :
		device(device),
		frame_config(frame_config),
		command_pool(CreateCommandPool(device.GetDevice(), device.GetQueueIndex(VK_QUEUE_GRAPHICS_BIT))),
		descriptor_pool(CreateDescriptorPool(device.GetDevice())),
		fence(CreateFence(device.GetDevice()))
	{
	}

	FrameDrawer::~FrameDrawer()
	{
		vkDestroyCommandPool(device.GetDevice(), command_pool, nullptr);
		vkDestroyDescriptorPool(device.GetDevice(), descriptor_pool, nullptr);
		vkDestroyFence(device.GetDevice(), fence, nullptr);
	}

	void FrameDrawer::Draw(SceneManager const * scene, vector<FrameDraw> const & draws, Frame const * frame)
	{
#pragma region // command buffer

		VkCommandBuffer command_buffer{};
		VkCommandBufferAllocateInfo command_pool_allocate_info{
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
			.commandPool = command_pool,
			.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
			.commandBufferCount = 1,
		};
		vkAllocateCommandBuffers(device.GetDevice(), &command_pool_allocate_info, &command_buffer);

		VkCommandBufferBeginInfo command_buffer_begin_info{.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
		vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info);

#pragma region // viewport & scissor

		VkViewport viewport{
			.x = 0.0,
			.y = 0.0,
			.width = static_cast<f32>(frame->GetExtent().width),
			.height = static_cast<f32>(frame->GetExtent().height),
			.minDepth = 0.0,
			.maxDepth = 1.0,
		};
		vkCmdSetViewport(command_buffer, 0, 1, &viewport);
		VkRect2D scissor{
			.offset = VkOffset2D{0, 0},
			.extent = frame->GetExtent(),
		};
		vkCmdSetScissor(command_buffer, 0, 1, &scissor);

#pragma region // render pass

		Array<VkClearValue, 2> clear_values{
			VkClearValue{.color = VkClearColorValue{.float32 = {0.5, 0.0, 0.5, 1.0}}},
			VkClearValue{.depthStencil = VkClearDepthStencilValue{.depth = 1, .stencil = 0}},
		};
		VkRenderPassBeginInfo render_pass_begin_info{
			.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
			.renderPass = frame_config.GetRenderPass(),
			.framebuffer = frame->GetFramebuffer(),
			.renderArea = VkRect2D{
				.offset = VkOffset2D{0, 0},
				.extent = frame->GetExtent(),
			},
			.clearValueCount = clear_values.Count(),
			.pClearValues = clear_values.elements,
		};
		vkCmdBeginRenderPass(command_buffer, &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);

#pragma region // commmon descriptors

		auto common_descriptor_set_layout = frame_config.GetCommonDescriptorSetLayout();
		VkDescriptorSetAllocateInfo common_descriptor_set_allocate_info{
			.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
			.descriptorPool = descriptor_pool,
			.descriptorSetCount = 1,
			.pSetLayouts = &common_descriptor_set_layout,
		};
		VkDescriptorSet common_descriptor_set{};
		vkAllocateDescriptorSets(device.GetDevice(), &common_descriptor_set_allocate_info, &common_descriptor_set);

		auto lights_descriptor_buffer_info = scene->lights.GetDescriptorInfo();
		auto surface_descriptor_buffer_info = scene->surfaces.GetDescriptorInfo();
		auto cameras_descriptor_buffer_info = scene->cameras.GetDescriptorInfo();
		Array<VkWriteDescriptorSet, 3> common_write_descriptor_set{
			VkWriteDescriptorSet{
				.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
				.dstSet = common_descriptor_set,
				.dstBinding = 0,
				.dstArrayElement = 0,
				.descriptorCount = 1,
				.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
				.pBufferInfo = &lights_descriptor_buffer_info,
			},
			VkWriteDescriptorSet{
				.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
				.dstSet = common_descriptor_set,
				.dstBinding = 1,
				.dstArrayElement = 0,
				.descriptorCount = 1,
				.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
				.pBufferInfo = &surface_descriptor_buffer_info,
			},
			VkWriteDescriptorSet{
				.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
				.dstSet = common_descriptor_set,
				.dstBinding = 2,
				.dstArrayElement = 0,
				.descriptorCount = 1,
				.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
				.pBufferInfo = &cameras_descriptor_buffer_info,
			},
		};
		vkUpdateDescriptorSets(device.GetDevice(), common_write_descriptor_set.Count(), common_write_descriptor_set.elements, 0, nullptr);

#pragma region // instances opaque

		Pipeline const * last_pipeline = nullptr;
		for (auto & draw : draws)
		{
			auto * pipeline = draw.pipeline;

			if (pipeline != last_pipeline)
			{
				vkCmdBindPipeline(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->GetPipeline());
				last_pipeline = pipeline;
			}

			auto * mesh = draw.mesh;
			auto vertex_buffer = mesh->GetVertexBuffer();
			usize vertex_buffer_offset = 0;
			vkCmdBindVertexBuffers(command_buffer, 0, 1, &vertex_buffer, &vertex_buffer_offset);
			auto index_buffer = mesh->GetIndexBuffer();
			vkCmdBindIndexBuffer(command_buffer, index_buffer, 0, VK_INDEX_TYPE_UINT32);

			auto instance_descriptor_set_layout = pipeline->GetInstanceDescriptorSetLayout();
			VkDescriptorSetAllocateInfo instance_descriptor_set_allocate_info{
				.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
				.descriptorPool = descriptor_pool,
				.descriptorSetCount = 1,
				.pSetLayouts = &instance_descriptor_set_layout,
			};
			VkDescriptorSet instance_descriptor_set{};
			vkAllocateDescriptorSets(device.GetDevice(), &instance_descriptor_set_allocate_info, &instance_descriptor_set);

			auto * material = draw.material;

			auto material_descriptor_buffer_info = material->GetDescriptorInfo();

			vector<VkWriteDescriptorSet> common_write_descriptor_set{
				VkWriteDescriptorSet{
					.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
					.dstSet = instance_descriptor_set,
					.dstBinding = 10,
					.dstArrayElement = 0,
					.descriptorCount = 1,
					.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
					.pBufferInfo = &material_descriptor_buffer_info,
				},
			};
			vector<unique_ptr<VkDescriptorImageInfo>> descriptor_image_infos;
			u32 image_array_index = 0;
			for (auto * image : draw.images)
			{
				auto & image_descriptor_image_info = descriptor_image_infos.emplace_back(make_unique<VkDescriptorImageInfo>(image->GetDescriptorInfo()));

				common_write_descriptor_set.emplace_back(
					VkWriteDescriptorSet{
						.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
						.dstSet = instance_descriptor_set,
						.dstBinding = 11,
						.dstArrayElement = image_array_index,
						.descriptorCount = 1,
						.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
						.pImageInfo = image_descriptor_image_info.get(),
					});
				image_array_index += 1;
			}

			vkUpdateDescriptorSets(device.GetDevice(), common_write_descriptor_set.size(), common_write_descriptor_set.data(), 0, nullptr);
			Array<VkDescriptorSet, 2> descritpro_sets{common_descriptor_set, instance_descriptor_set};
			vkCmdBindDescriptorSets(
				command_buffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				pipeline->GetPipelineLayout(),
				0,
				descritpro_sets.Count(),
				descritpro_sets.elements,
				0,
				nullptr);

			ObjectIndices object_indices{
				.light_count = static_cast<u16>(draw.lights.size()),
				.surface = static_cast<u16>(draw.surface->index.index),
				.camera = static_cast<u16>(draw.camera->index.index),
			};
			u32 light_index = 0;
			for (auto * light : draw.lights)
			{
				object_indices.lights[light_index] = static_cast<u16>(light->index.index);
				light_index += 1;
			}
			vkCmdPushConstants(command_buffer, pipeline->GetPipelineLayout(), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(ObjectIndices), &object_indices);

			vkCmdDrawIndexed(command_buffer, mesh->GetIndexCount() * 3, 1, 0, 0, 0);
		}

#pragma endregion

#pragma endregion

		vkCmdEndRenderPass(command_buffer);

#pragma endregion

#pragma endregion

		vkEndCommandBuffer(command_buffer);

		auto graphics_queue_index = device.GetQueueIndex(VK_QUEUE_GRAPHICS_BIT);
		VkSubmitInfo submit_info{
			.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
			.commandBufferCount = 1,
			.pCommandBuffers = &command_buffer,
		};
		device.SubmitToQueue(graphics_queue_index, submit_info, fence);

		vkWaitForFences(device.GetDevice(), 1, &fence, VK_TRUE, UINT64_MAX);
		vkResetFences(device.GetDevice(), 1, &fence);

		vkResetDescriptorPool(device.GetDevice(), descriptor_pool, 0);
		vkFreeCommandBuffers(device.GetDevice(), command_pool, 1, &command_buffer);
		vkResetCommandPool(device.GetDevice(), command_pool, 0);

#pragma endregion
	}

	VkCommandPool FrameDrawer::CreateCommandPool(VkDevice device, u32 queue_index)
	{
		VkCommandPoolCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
			.queueFamilyIndex = queue_index,
		};

		VkCommandPool command_pool{};
		vkCreateCommandPool(device, &create_info, nullptr, &command_pool);
		return command_pool;
	}

	VkDescriptorPool FrameDrawer::CreateDescriptorPool(VkDevice device)
	{
		Array<VkDescriptorPoolSize, 5> descriptor_pool_sizes{
			// lights
			VkDescriptorPoolSize{
				.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
				.descriptorCount = 1,
			},
			// surfaces
			VkDescriptorPoolSize{
				.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
				.descriptorCount = 1,
			},
			// cameras
			VkDescriptorPoolSize{
				.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
				.descriptorCount = 1,
			},
			// material
			VkDescriptorPoolSize{
				.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
				.descriptorCount = 1024 * 4,
			},
			// textures
			VkDescriptorPoolSize{
				.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
				.descriptorCount = 1024 * 4,
			},
		};

		VkDescriptorPoolCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
			.maxSets = 1024 * 2 * 2,
			.poolSizeCount = descriptor_pool_sizes.Count(),
			.pPoolSizes = descriptor_pool_sizes.elements,
		};

		VkDescriptorPool descriptor_pool{};
		vkCreateDescriptorPool(device, &create_info, nullptr, &descriptor_pool);
		return descriptor_pool;
	}

	VkFence FrameDrawer::CreateFence(VkDevice device)
	{
		VkFenceCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
		};
		VkFence fence;
		vkCreateFence(device, &create_info, nullptr, &fence);
		return fence;
	}
}
