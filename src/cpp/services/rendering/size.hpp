#pragma once

#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::rendering
{
	struct Size
	{
		u32 width{};
		u32 height{};

		bool operator==(Size other)
		{
			return width == other.width && height == other.height;
		}
		bool operator!=(Size other)
		{
			return !(*this == other);
		}
	};
}
