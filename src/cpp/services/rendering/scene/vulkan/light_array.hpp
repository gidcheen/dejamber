#pragma once

#include <vulkan/vulkan.h>

#include "../../data/light.hpp"
#include "object_array.hpp"

namespace gid_tech::rendering::implementation
{
	class LightArray : public ObjectArray<data::Light>
	{
	public:
		LightArray(Device & device, MemoryCache & memory_cache, MemoryTransfer & memory_transfer) :
			ObjectArray<data::Light>(device, memory_cache, memory_transfer, 1024)
		{
		}
	};
}
