#pragma once

#include <algorithm>
#include <cassert>
#include <cstring>

#include "object_array.hpp"

namespace gid_tech::rendering::implementation
{
	using std::find;

	template<typename T>
	ObjectArray<T>::ObjectArray(Device & device, MemoryCache & memory_cache, MemoryTransfer & memory_transfer, u32 initial_size) :
		device(device),
		memory_cache(memory_cache),
		memory_transfer(memory_transfer),
		free(initial_size),
		data(initial_size),
		buffer(CreateBuffer(device.GetDevice(), initial_size * sizeof(T))),
		memory(CreateDeviceMemory(
			device.GetDevice(),
			buffer,
			device.GetMemoryIndex(GetMemoryRequirements(device.GetDevice(), buffer).memoryTypeBits, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
			GetMemoryRequirements(device.GetDevice(), buffer).size))
	{
		for (u32 i = 0; i < initial_size; i++)
			free[i] = Index{initial_size - i - 1};
		//memory_cache.Bind(buffer, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	}

	template<typename T>
	ObjectArray<T>::~ObjectArray()
	{
		//memory_cache.Unbind(buffer);
		vkFreeMemory(device.GetDevice(), memory, nullptr);
		vkDestroyBuffer(device.GetDevice(), buffer, nullptr);
	}

	template<typename T>
	VkDescriptorBufferInfo ObjectArray<T>::GetDescriptorInfo() const
	{
		return VkDescriptorBufferInfo{
			.buffer = buffer,
			.offset = 0,
			.range = VK_WHOLE_SIZE,
		};
	}

	template<typename T>
	typename ObjectArray<T>::Index ObjectArray<T>::Add(T const & value)
	{
		assert(free.size() > 0);

		auto index = free.back();
		free.pop_back();
		Set(index, value);
		return index;
	}

	template<typename T>
	void ObjectArray<T>::Remove(Index index)
	{
		assert(find(free.begin(), free.end(), index) == free.end());
		free.emplace_back(index);
	}

	template<typename T>
	T const & ObjectArray<T>::Get(Index index)
	{
		return data[index.index];
	}

	template<typename T>
	void ObjectArray<T>::Set(Index index, T const & value)
	{
		data[index.index] = value;

		VkDeviceSize offset = index.index * sizeof(T);
		VkDeviceSize size = sizeof(T);

		u8 * data{};
		vkMapMemory(device.GetDevice(), memory, 0, VK_WHOLE_SIZE, 0, (void **) &data);
		memcpy(data + offset, &value, size);
		VkMappedMemoryRange mapped_memory_range{
			.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
			.memory = memory,
			.offset = 0,
			.size = VK_WHOLE_SIZE,
		};
		vkFlushMappedMemoryRanges(device.GetDevice(), 1, &mapped_memory_range);
		vkUnmapMemory(device.GetDevice(), memory);

		// memory_transfer.Transfer(
		// 	MemoryTransfer::SrcData{
		// 		.ptr = &data[index.index],
		// 		.size = sizeof(T),
		// 	},
		// 	MemoryTransfer::DstBufferData{
		// 		.buffer = buffer,
		// 		.offset = index.index * sizeof(T),
		// 	});
	}

	template<typename T>
	VkBuffer ObjectArray<T>::CreateBuffer(VkDevice device, usize size)
	{
		VkBufferCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
			.size = size,
			.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		};
		VkBuffer buffer{};
		vkCreateBuffer(device, &create_info, nullptr, &buffer);
		return buffer;
	}

	template<typename T>
	VkMemoryRequirements ObjectArray<T>::GetMemoryRequirements(VkDevice device, VkBuffer buffer)
	{
		VkMemoryRequirements memory_requirements{};
		vkGetBufferMemoryRequirements(device, buffer, &memory_requirements);
		return memory_requirements;
	}

	template<typename T>
	VkDeviceMemory ObjectArray<T>::CreateDeviceMemory(VkDevice device, VkBuffer buffer, u32 memory_type_index, VkDeviceSize size)
	{
		VkMemoryAllocateInfo allocate_info{
			.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
			.allocationSize = size,
			.memoryTypeIndex = memory_type_index,
		};

		VkDeviceMemory memory{};
		vkAllocateMemory(device, &allocate_info, nullptr, &memory);
		vkBindBufferMemory(device, buffer, memory, 0);
		return memory;
	}
}
