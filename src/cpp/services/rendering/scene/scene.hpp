#pragma once

#include <hidden/hidden.hpp>

#include "../render_manager.hpp"

using namespace std;

namespace gid_tech::rendering::implementation
{
	class SceneManager;
}

namespace gid_tech::rendering
{
	using SceneManagerImplementation = Hidden<implementation::SceneManager, 264>;

	class SceneManager : public SceneManagerImplementation
	{
	public:
		SceneManager(RenderManager & render_manager);
		~SceneManager();

		SceneManager(SceneManager const &) = delete;
		SceneManager & operator=(SceneManager const &) = delete;
	};
}
