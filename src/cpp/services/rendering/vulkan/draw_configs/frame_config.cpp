#include "frame_config.hpp"

#include <collections/array.hpp>

using namespace gid_tech::collections;

namespace gid_tech::rendering::implementation
{
	FrameConfig::FrameConfig(Device & device) :
		device(device),
		render_pass(CreateRenderPass(device.GetDevice())),
		common_descriptor_set_layout(CreateCommonDescriptorSetLayout(device.GetDevice()))
	{
	}

	FrameConfig::~FrameConfig()
	{
		vkDestroyRenderPass(device.GetDevice(), render_pass, nullptr);
		vkDestroyDescriptorSetLayout(device.GetDevice(), common_descriptor_set_layout, nullptr);
	}

	VkRenderPass FrameConfig::GetRenderPass() const
	{
		return render_pass;
	}

	VkDescriptorSetLayout FrameConfig::GetCommonDescriptorSetLayout() const
	{
		return common_descriptor_set_layout;
	}

	VkRenderPass FrameConfig::CreateRenderPass(VkDevice device)
	{
		Array<VkAttachmentDescription, 2> attachment_descriptions{
			VkAttachmentDescription{
				.format = VK_FORMAT_R32G32B32A32_SFLOAT,
				.samples = VK_SAMPLE_COUNT_1_BIT,
				.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
				.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
				.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
				.stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE,
				.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
				.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
			},
			VkAttachmentDescription{
				.format = VK_FORMAT_D32_SFLOAT,
				.samples = VK_SAMPLE_COUNT_1_BIT,
				.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
				.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
				.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
				.stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE,
				.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
				.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
			},
		};

		Array<Array<VkAttachmentReference, 1>, 1> color_attachments{
			Array<VkAttachmentReference, 1>{
				VkAttachmentReference{
					.attachment = 0,
					.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
				},
			},
		};

		Array<VkAttachmentReference, 1> depth_attachment{
			VkAttachmentReference{
				.attachment = 1,
				.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
			},
		};

		Array<VkSubpassDescription, 1> subpass_descriptions{
			VkSubpassDescription{
				.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
				.colorAttachmentCount = static_cast<uint32_t>(color_attachments[0].Count()),
				.pColorAttachments = color_attachments[0].elements,
				.pDepthStencilAttachment = &depth_attachment[0],
			},
		};

		VkRenderPassCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
			.attachmentCount = static_cast<uint32_t>(attachment_descriptions.Count()),
			.pAttachments = attachment_descriptions.elements,
			.subpassCount = static_cast<uint32_t>(subpass_descriptions.Count()),
			.pSubpasses = subpass_descriptions.elements,
		};

		VkRenderPass render_pass{};
		vkCreateRenderPass(device, &create_info, nullptr, &render_pass);
		return render_pass;
	}

	VkDescriptorSetLayout FrameConfig::CreateCommonDescriptorSetLayout(VkDevice device)
	{
		Array<VkDescriptorSetLayoutBinding, 3> bindings{
			// lights
			VkDescriptorSetLayoutBinding{
				.binding = 0,
				.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
			},
			// surfaces
			VkDescriptorSetLayoutBinding{
				.binding = 1,
				.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, //VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
			},
			// cameras
			VkDescriptorSetLayoutBinding{
				.binding = 2,
				.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, //VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
			},
		};

		VkDescriptorSetLayoutCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
			.bindingCount = static_cast<uint32_t>(bindings.Count()),
			.pBindings = bindings.elements,
		};

		VkDescriptorSetLayout descriptor_set_layout{};
		vkCreateDescriptorSetLayout(device, &create_info, nullptr, &descriptor_set_layout);
		return descriptor_set_layout;
	}
}
