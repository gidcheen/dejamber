#pragma once

#include <string>

#include "device.hpp"
#include "draw_configs/frame_config.hpp"
#include "memory/memory_block.hpp"
#include "memory/memory_cache.hpp"
#include "memory/memory_transfer.hpp"

namespace gid_tech::rendering::implementation
{
	using std::string;

	class RenderManager
	{
	public:
		Device device;
		MemoryCache memory_cache;
		MemoryTransfer memory_transfer;
		FrameConfig frame_config;

	public:
		RenderManager(string const & identifier);
	};
}
