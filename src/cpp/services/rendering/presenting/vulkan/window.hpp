#pragma once

#include <tuple>

#include <vulkan/vulkan.h>
#undef CreateSemaphore

#include "../../size.hpp"
#include "../../targets/vulkan/frame.hpp"
#include "../../vulkan/device.hpp"

namespace gid_tech::rendering::implementation
{
	using std::tuple;

	class Window
	{
	public:
		struct NativeWindowData
		{
			void * window_manager{};
			void * window{};
		};

	private:
		Device & device;

		VkSurfaceKHR const surface;
		VkSwapchainKHR swapchain;
		VkExtent2D swapchain_extent;
		VkCommandPool command_pool;
		VkSemaphore const image_aquired_semaphore;
		VkSemaphore const image_presented_semaphore;

		VkFence const aquire_fence;
		VkFence const present_fence;

	public:
		Window(Device & device, NativeWindowData native_window_data);
		~Window();

		Window(Window const &) = delete;
		Window & operator=(Window const &) = delete;


		VkExtent2D GetExtent();

		void Present(Frame const & frame);

	private:
		void Present(VkImage image, VkExtent2D extent);

	private:
		static VkSurfaceKHR CreateSurface(VkInstance instance, NativeWindowData native_window_data);
		static tuple<VkSwapchainKHR, VkExtent2D> CreateSwapchain(VkPhysicalDevice physical_device, VkDevice device, VkSurfaceKHR surface, VkSwapchainKHR old_swapchain);
		static VkCommandPool CreateCommandPool(VkDevice device, u32 queue_index);
		static VkSemaphore CreateSemaphore(VkDevice device);
		static VkFence CreateFence(VkDevice device);
	};
}
