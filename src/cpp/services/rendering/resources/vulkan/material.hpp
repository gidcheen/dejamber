#pragma once

#include <vector>

#include <vulkan/vulkan.h>

#include "../../vulkan/device.hpp"
#include "../../vulkan/memory/memory_cache.hpp"
#include "../../vulkan/memory/memory_transfer.hpp"

using namespace std;

namespace gid_tech::rendering::implementation
{
	class Material
	{
	private:
		Device & device;
		MemoryCache & memory_cache;
		MemoryTransfer & memory_transfer;

		usize size;
		vector<u32> data;

		VkBuffer const buffer;

	public:
		Material(Device & device, MemoryCache & memory_cache, MemoryTransfer & memory_transfer, vector<u32> data);
		~Material();

		Material(Material const &) = delete;
		Material & operator=(Material const &) = delete;


		VkDescriptorBufferInfo GetDescriptorInfo() const;

		vector<u32> const & Get() const;
		void Set(vector<u32> data);

	private:
		static VkBuffer CreateBuffer(VkDevice device, usize size);
	};
}
