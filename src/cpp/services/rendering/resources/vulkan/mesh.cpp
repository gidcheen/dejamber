#include "mesh.hpp"

#include <cassert>

#include "../../vulkan/render_manager.hpp"
#include "../mesh.hpp"

namespace gid_tech::rendering
{
	using std::move;

	Mesh::Mesh(RenderManager & render_manager, vector<data::Vertex> vertices, vector<data::Index> indices) :
		MeshImplementation(render_manager.device, render_manager.memory_cache, render_manager.memory_transfer, move(vertices), move(indices))
	{
	}

	Mesh::~Mesh()
	{
	}

	vector<data::Vertex> const & Mesh::GetVertices() const
	{
		return MeshImplementation::GetVertices();
	}

	vector<data::Index> const & Mesh::GetIndices() const
	{
		return MeshImplementation::GetIndices();
	}

	void Mesh::Set(vector<data::Vertex> vertices, vector<data::Index> indices)
	{
		MeshImplementation::Set(move(vertices), move(indices));
	}

	Mesh::Bounds const & Mesh::GetBounds()
	{
		return reinterpret_cast<Mesh::Bounds const &>(MeshImplementation::GetBounds());
	}
}

namespace gid_tech::rendering::implementation
{
	using std::move;

	Mesh::Mesh(Device & device, MemoryCache & memory_cache, MemoryTransfer & memory_transfer, vector<data::Vertex> vertices, vector<data::Index> indices) :
		device(device),
		memory_cache(memory_cache),
		memory_transfer(memory_transfer),
		vertex_count(vertices.size()),
		index_count(indices.size()),
		vertices(),
		indices(),
		vertex_buffer(CreateVertexBuffer(device.GetDevice(), vertex_count * sizeof(data::Vertex))),
		index_buffer(CreateIndexBuffer(device.GetDevice(), index_count * sizeof(data::Index)))
	{
		memory_cache.Bind(vertex_buffer, VK_MEMORY_HEAP_DEVICE_LOCAL_BIT);
		memory_cache.Bind(index_buffer, VK_MEMORY_HEAP_DEVICE_LOCAL_BIT);
		Set(move(vertices), move(indices));
	}

	Mesh::~Mesh()
	{
		memory_cache.Unbind(vertex_buffer);
		memory_cache.Unbind(index_buffer);
		vkDestroyBuffer(device.GetDevice(), vertex_buffer, nullptr);
		vkDestroyBuffer(device.GetDevice(), index_buffer, nullptr);
	}

	VkBuffer Mesh::GetVertexBuffer() const
	{
		return vertex_buffer;
	}

	VkBuffer Mesh::GetIndexBuffer() const
	{
		return index_buffer;
	}

	u32 Mesh::GetIndexCount() const
	{
		return index_count;
	}

	vector<data::Vertex> const & Mesh::GetVertices() const
	{
		return vertices;
	}

	vector<data::Index> const & Mesh::GetIndices() const
	{
		return indices;
	}

	Mesh::Bounds const & Mesh::GetBounds()
	{
		return bounds;
	}

	void Mesh::Set(vector<data::Vertex> vertices, vector<data::Index> indices)
	{
		assert(vertices.size() == vertex_count);
		assert(indices.size() == index_count);

		this->vertices = move(vertices);
		this->indices = move(indices);

		memory_transfer.Transfer(
			MemoryTransfer::SrcData{
				.ptr = this->vertices.data(),
				.size = this->vertices.size() * sizeof(data::Vertex),
			},
			MemoryTransfer::DstBufferData{
				.buffer = vertex_buffer,
				.offset = 0,
			});
		memory_transfer.Transfer(
			MemoryTransfer::SrcData{
				.ptr = this->indices.data(),
				.size = this->indices.size() * sizeof(data::Index),
			},
			MemoryTransfer::DstBufferData{
				.buffer = index_buffer,
				.offset = 0,
			});

		bounds.min = this->vertices[0].position;
		bounds.max = this->vertices[0].position;
		for (usize i = 1; i < vertex_count; i++)
		{
			bounds.min = bounds.min.Min(this->vertices[i].position);
			bounds.max = bounds.max.Max(this->vertices[i].position);
		}
	}

	VkBuffer Mesh::CreateVertexBuffer(VkDevice device, usize size)
	{
		VkBufferCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
			.size = size,
			.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		};
		VkBuffer buffer{};
		vkCreateBuffer(device, &create_info, nullptr, &buffer);
		return buffer;
	}

	VkBuffer Mesh::CreateIndexBuffer(VkDevice device, usize size)
	{
		VkBufferCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
			.size = size,
			.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		};
		VkBuffer buffer{};
		vkCreateBuffer(device, &create_info, nullptr, &buffer);
		return buffer;
	}
}
