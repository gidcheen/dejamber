#pragma once

#include <vector>

#include "../render_manager.hpp"

using namespace std;

namespace gid_tech::rendering::implementation
{
	class Material;
}

namespace gid_tech::rendering
{
	using MaterialImplementation = Hidden<implementation::Material, 64>;

	class Material : public MaterialImplementation
	{
	public:
		Material(RenderManager & render_manager, vector<u32> data);
		~Material();

		Material(Material const &) = delete;
		Material & operator=(Material const &) = delete;


		vector<u32> const & Get() const;
		void Set(vector<u32> data);
	};
}
