#pragma once

#include <vector>

#include "../data/pixel.hpp"
#include "../render_manager.hpp"
#include "../size.hpp"

namespace gid_tech::rendering::implementation
{
	class Image;
}

namespace gid_tech::rendering
{
	using namespace std;
	
	using ImageImplementation = Hidden<implementation::Image, 80>;

	class Image : public ImageImplementation
	{
	public:
		Image(RenderManager & render_manager, Size size, vector<data::Pixel> pixels);
		~Image();

		Image(Image const &) = delete;
		Image operator=(Image const &) = delete;

		Size GetSize() const;
		vector<data::Pixel> const & GetPixels() const;
		void SetPixels(vector<data::Pixel> pixels);
	};
}
