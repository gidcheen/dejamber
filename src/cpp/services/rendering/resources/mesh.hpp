#pragma once

#include <vector>

#include <algebra/vector.hpp>

#include "../data/mesh.hpp"
#include "../render_manager.hpp"

namespace gid_tech::rendering::implementation
{
	class Mesh;
}

namespace gid_tech::rendering
{
	using namespace std;
	using namespace algebra;

	using MeshImplementation = Hidden<implementation::Mesh, 128>;

	class Mesh : public MeshImplementation
	{
	public:
		struct Bounds
		{
			Vector<f32, 3> min;
			Vector<f32, 3> max;
		};

	public:
		Mesh(RenderManager & render_manager, vector<data::Vertex> vertices, vector<data::Index> indices);
		~Mesh();

		Mesh(Mesh const &) = delete;
		Mesh & operator=(Mesh const &) = delete;


		vector<data::Vertex> const & GetVertices() const;
		vector<data::Index> const & GetIndices() const;
		void Set(vector<data::Vertex> vertices, vector<data::Index> indices);

		Bounds const & GetBounds();
	};
}
