#ifdef LINUX

#include "syscalls.hpp"

namespace gid_tech::linux
{
	usize Syscall(usize number)
	{
		usize ret;
		asm volatile(
			"syscall"
			: "=r"(ret)
			: "a"(number)
			: "rcx", "r11", "memory");
		return ret;
	}

	usize Syscall(usize number, usize arg1)
	{
		usize ret;
		asm volatile(
			"syscall"
			: "=r"(ret)
			: "a"(number), "D"(arg1)
			: "rcx", "r11", "memory");
		return ret;
	}

	usize Syscall(usize number, usize arg1, usize arg2)
	{
		usize ret;
		asm volatile(
			"syscall"
			: "=r"(ret)
			: "a"(number), "D"(arg1), "S"(arg2)
			: "rcx", "r11", "memory");
		return ret;
	}

	usize Syscall(usize number, usize arg1, usize arg2, usize arg3)
	{
		usize ret;
		asm volatile(
			"syscall"
			: "=r"(ret)
			: "a"(number), "D"(arg1), "S"(arg2), "d"(arg3)
			: "rcx", "r11", "memory");
		return ret;
	}

	usize Syscall(usize number, usize arg1, usize arg2, usize arg3, usize arg4)
	{
		usize ret;
		register usize r10 asm("r10") = arg4;
		asm volatile(
			"syscall"
			: "=r"(ret)
			: "a"(number), "D"(arg1), "S"(arg2), "d"(arg3), "r"(r10)
			: "rcx", "r11", "memory");
		return ret;
	}

	usize Syscall(usize number, usize arg1, usize arg2, usize arg3, usize arg4, usize arg5)
	{
		usize ret;
		register long r10 asm("r10") = arg4;
		register long r8 asm("r8") = arg5;
		asm volatile(
			"syscall"
			: "=r"(ret)
			: "a"(number), "D"(arg1), "S"(arg2), "d"(arg3), "r"(r10), "r"(r8)
			: "rcx", "r11", "memory");
		return ret;
	}

	usize Syscall(usize number, usize arg1, usize arg2, usize arg3, usize arg4, usize arg5, usize arg6)
	{
		usize ret;
		register long r10 asm("r10") = arg4;
		register long r8 asm("r8") = arg5;
		register long r9 asm("r9") = arg6;
		asm volatile(
			"syscall"
			: "=r"(ret)
			: "a"(number), "D"(arg1), "S"(arg2), "d"(arg3), "r"(r10), "r"(r8), "r"(r9)
			: "rcx", "r11", "memory");
		return ret;
	}
}

#endif
