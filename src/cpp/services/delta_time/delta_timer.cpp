#include "delta_timer.hpp"

namespace gid_tech::delta_time
{
#ifdef LINUX

#include <sys/time.h>

	static u64 GetTimeValue()
	{
		timeval tv;
		gettimeofday(&tv, nullptr);
		return (u64) tv.tv_sec * (u64) 1000000 + (u64) tv.tv_usec;
	}

	static f64 const frequency = 1000000;
	static u64 offset = GetTimeValue();

	static f64 GetTime()
	{
		return (f64)(GetTimeValue() - offset) / frequency;
	}

#elif WINDOWS

#include <Windows.h>

	static LARGE_INTEGER GetFrequency()
	{
		LARGE_INTEGER frequency;
		QueryPerformanceFrequency(&frequency);
		return frequency;
	}

	static LARGE_INTEGER GetOffset()
	{
		LARGE_INTEGER offset;
		QueryPerformanceCounter(&offset);
		return offset;
	}

	static LARGE_INTEGER frequency = GetFrequency();
	static LARGE_INTEGER offset = GetOffset();

	static f64 GetTime()
	{
		LARGE_INTEGER value;
		QueryPerformanceCounter(&value);
		return (f64)(value.QuadPart - offset.QuadPart) / (f64) frequency.QuadPart;
	}

#endif
	DeltaTimer::DeltaTimer() :
		time_points{
			0,
			GetTime() - (1 * 1.0 / 60.0),
			GetTime() - (2 * 1.0 / 60.0),
		}
	{
	}

	f32 DeltaTimer::Step()
	{
		auto current_index = last_index;
		last_index = (last_index + 1) % time_points.Count();

		time_points[current_index] = GetTime();

		return (time_points[current_index] - time_points[last_index]) / time_points.Count();
	}
}
