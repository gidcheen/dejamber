#pragma once

#include <filesystem>
#include <map>

namespace gid_tech::file_watch
{
	using namespace std;
	using namespace filesystem;

	enum class FileState
	{
		Added,
		Modified,
		Removed
	};

	class FileWatcher
	{
	private:
		filesystem::path tracked_path;
		map<string, file_time_type> tracked_files;

	public:
		FileWatcher(string path);

		map<string, FileState> Update();
	};
}
