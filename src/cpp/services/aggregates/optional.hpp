#pragma once

#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::aggregates
{
	struct OptionalNoneVal
	{
	};
	constexpr OptionalNoneVal OptionalNone = OptionalNoneVal{};

	template<typename T>
	class Optional final
	{
	private:
		union
		{
			T value;
		};
		b8 is_some{};

	public:
		template<typename... Args>
		constexpr Optional(Args... args);
		constexpr Optional(OptionalNoneVal);

		constexpr Optional(Optional<T> const & other);
		constexpr Optional(Optional<T> && other);

		~Optional();

		Optional<T> & operator=(Optional<T> const & other);
		Optional<T> & operator=(Optional<T> && other);
		Optional<T> & operator=(OptionalNoneVal);

		explicit operator bool() const;

		b8 operator==(Optional<T> & other) const;
		b8 operator!=(Optional<T> & other) const;

		T & operator*();
		T const & operator*() const;
		T * operator->();
		T const * operator->() const;

		constexpr T & Value();
		constexpr T const & Value() const;

		constexpr b8 IsSome() const;
		constexpr b8 IsNone() const;

		template<typename... Args>
		void Emplace(Args &&... args); // todo rename
		void Clear();
	};
}

#include "optional.impl.hpp"
