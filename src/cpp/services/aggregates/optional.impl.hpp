#pragma once

#include "optional.hpp"

#include <heap/new.hpp>
#include <types/traits.hpp>

namespace gid_tech::aggregates
{
	template<typename T>
	template<typename... Args>
	constexpr Optional<T>::Optional(Args... args) :
		is_some(true)
	{
		new (&value) T(args...);
	}

	template<typename T>
	constexpr Optional<T>::Optional(OptionalNoneVal)
	{
	}

	template<typename T>
	constexpr Optional<T>::Optional(Optional<T> const & other) :
		is_some(other.is_some)
	{
		if (other.is_some)
			new (&value) T(other.value);
	}

	template<typename T>
	constexpr Optional<T>::Optional(Optional<T> && other) :
		is_some(other.is_some)
	{
		if (other.is_some)
			new (&value) T(static_cast<T &&>(other.value));
	}

	template<typename T>
	Optional<T>::~Optional()
	{
		if (is_some)
			value.~T();
	}

	template<typename T>
	Optional<T> & Optional<T>::operator=(Optional<T> const & other)
	{
		if (is_some && other.is_some)
			value = other.value;
		else if (!is_some && other.is_some)
			new (&value) T(other.value);
		else if (is_some && !other.is_some)
			value.~T();

		is_some = other.is_some;

		return *this;
	}

	template<typename T>
	Optional<T> & Optional<T>::operator=(Optional<T> && other)
	{
		if (is_some && other.is_some)
			value = static_cast<T &&>(other.value);
		else if (!is_some && other.is_some)
			new (&value) T(static_cast<T &&>(other.value));
		else if (is_some && !other.is_some)
			value.~T();

		is_some = other.is_some;

		return *this;
	}

	template<typename T>
	Optional<T> & Optional<T>::operator=(OptionalNoneVal)
	{
		if (is_some)
			value.~T();
		is_some = false;
		return *this;
	}

	template<typename T>
	Optional<T>::operator bool() const
	{
		return is_some;
	}

	template<typename T>
	b8 Optional<T>::operator==(Optional<T> & other) const
	{
		b8 is_same = is_some == other.is_some;
		if (is_some && other.is_some)
			is_same &= value == other.value;
		return is_same;
	}

	template<typename T>
	b8 Optional<T>::operator!=(Optional<T> & other) const
	{
		b8 is_not_same = is_some != other.is_some;
		if (is_some && other.is_some)
			is_not_same &= value != other.value;
		return is_not_same;
	}

	template<typename T>
	T & Optional<T>::operator*()
	{
		return value;
	}

	template<typename T>
	T const & Optional<T>::operator*() const
	{
		return value;
	}

	template<typename T>
	T * Optional<T>::operator->()
	{
		return &value;
	}

	template<typename T>
	T const * Optional<T>::operator->() const
	{
		return &value;
	}

	template<typename T>
	constexpr T & Optional<T>::Value()
	{
		return value;
	}

	template<typename T>
	constexpr T const & Optional<T>::Value() const
	{
		return value;
	}

	template<typename T>
	constexpr b8 Optional<T>::IsSome() const
	{
		return is_some;
	}

	template<typename T>
	constexpr b8 Optional<T>::IsNone() const
	{
		return !is_some;
	}

	template<typename T>
	template<typename... Args>
	void Optional<T>::Emplace(Args &&... args)
	{
		if (is_some)
			value.~T();
		new (&value) T{args...};
		is_some = true;
	}

	template<typename T>
	void Optional<T>::Clear()
	{
		if (is_some)
			value.~T();
		is_some = false;
	}
}
