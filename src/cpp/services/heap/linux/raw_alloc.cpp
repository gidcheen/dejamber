#ifdef LINUX

#include "../raw_alloc.hpp"

#include <linux/syscalls.hpp>
#include <sys/mman.h>

using namespace gid_tech::linux;

namespace gid_tech::heap
{
	void * RawAlloc(usize size)
	{
		auto u_ptr = reinterpret_cast<usize *>(MMap(0, size + sizeof(usize), PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0));
		u_ptr[0] = size;
		return reinterpret_cast<void *>(u_ptr + 1);
	}

	void RawDealloc(void * ptr)
	{
		auto u_ptr = reinterpret_cast<usize *>(ptr) - 1;
		auto size = u_ptr[0];
		MUnmap(reinterpret_cast<usize>(u_ptr), size);
	}

	usize RawSize(void * ptr)
	{
		auto u_ptr = reinterpret_cast<usize *>(ptr) - 1;
		auto size = u_ptr[0];
		return size;
	}
}

#endif
