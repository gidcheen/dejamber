#pragma once

#include <collections/array.hpp>
#include <types/traits.hpp>
#include <types/types.hpp>

#include <heap/new.hpp>

using namespace gid_tech::types;
using namespace gid_tech::collections;

namespace gid_tech
{
	template<typename, typename = void>
	struct IsDefined : False
	{
	};
	template<typename T>
	struct IsDefined<T, typename EnableIf<IsClass<T>::value && !IsPointer<T>::value && (sizeof(T) > 0)>::Type> : True
	{
	};

	template<usize TSize>
	class HiddenBlock
	{
	private:
		Array<u8, TSize> data;
	};

	template<typename T, usize TSize>
	class Hidden : public If<IsDefined<T>::value, T, HiddenBlock<TSize>>::Type
	{
	public:
		template<typename... TArgs>
		Hidden(TArgs &&... args);
		~Hidden();
	};

	template<typename T, usize TSize>
	template<typename... TArgs>
	Hidden<T, TSize>::Hidden(TArgs &&... args) :
		T(args...)
	{
		static_assert(IsDefined<T>::value);
		static_assert(sizeof(T) == TSize);
	}

	template<typename T, usize TSize>
	inline Hidden<T, TSize>::~Hidden()
	{
		static_assert(IsDefined<T>::value);
	}
}
