# Lazy Rider

Entry for [DeJamber by Game Dev Graz](https://itch.io/jam/game-dev-graz-/results)

## [Download](https://gideonung.itch.io/la-z)

## Goals

- Collect **3** space margaritas
- Collect **5** space rocks

## Controls

- **left mouse:** *throw rock*
- **middle mouse:** *rotate camera*
- **right mouse:** *throw grappling hook*

## Screenshots
| | |
| ----------- | ----------- | 
| ![screenshot1](screenshots/screenshot1.png) | ![screenshot2](screenshots/screenshot2.png) |
| ![screenshot3](screenshots/screenshot3.png) | ![screenshot4](screenshots/screenshot4.png) |

## Building

dependencies
- dotnet core 3.1 (for the build tool)
- vulkan sdk (glslc in path)
- clang (clang in path)

run build.bat

*or*

run the build task in visual studio code

## Running

run `_target/bin/lazy_rider.exe` in `_target/bin` as cwd

*or*

run from visual studio code

## Packaging

rename `_target` to `lazy_rider` then zip and upload
